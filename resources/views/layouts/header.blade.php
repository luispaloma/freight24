<header id="this-is-top">
	<div class="container-fluid">
		<div class="topmenu row">
			<nav class="col-sm-offset-3 col-md-offset-4 col-lg-offset-4 col-sm-6 col-md-5 col-lg-5">
				@if ( Auth::check() )
					Logged in as:
					<a href="#">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a>
					<form action="{{ route('logout') }}" method="POST" style="display: inline-block;">
						{{ csrf_field() }}
						<button type="submit" class="">Log out</button>
					</form>
				@else
					<a href="{{ route('login') }}">LOG IN</a>
					<a href="{{ route('register') }}">REGISTRATION</a>
					<a href="#">ENGLISH</a>
				@endif
			</nav>
			<nav class="text-right col-sm-3 col-md-3 col-lg-3">
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-google-plus"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-pinterest"></i></a>
				<a href="#"><i class="fa fa-youtube"></i></a>
			</nav>
		</div>
		<div class="row header">
			<div class="col-sm-3 col-md-3 col-lg-3"> <a href="{{ route('index') }}" id="logo"></a> </div>
			<div class="col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-sm-8 col-md-8 col-lg-8">
				<div class="text-right header-padding"> <div class="h-block"><span>EMAIL US</span>info@i-freight24.com</div> </div>
			</div>
		</div>
		<div id="main-menu-bg"></div>
		<a id="menu-open" href="#"><i class="fa fa-bars"></i></a>
		<nav class="main-menu navbar-main-slide">
			<ul class="nav navbar-nav navbar-main">
				<li>
					<a href="{{ route('index') }}">HOME</a>
				</li>
				<li class="dropdown">
					<a data-toggle="dropdown" class="dropdown-toggle border-hover-color3" href="#">
						FOR SHIPPERS <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="#">SEA FREIGHT</a>
						</li>
							<ul>
								<li class="second_level_menu"><a href="{{ route('lclcontroller.index') }}">LCL</a></li>
								<li class="second_level_menu"><a href="{{ route('index.fcl') }}">FCL</a></li>
								<li class="second_level_menu"><a href="{{ route('index.sea') }}">in Bulk</a></li>
							</ul>
						<li>
							<a href="#">ROAD FREIGHT</a>
						</li>
							<ul>
								<li class="second_level_menu"><a href="{{ route('index.road_ltl') }}">Road Freight LTL</a></li>
								<li class="second_level_menu"><a href="{{ route('index.road_ftl') }}">Road Freight FTL</a></li>
							</ul>
						<li><a href="{{ route('index.air') }}">Airfreight</a></li>
						<li><a href="{{ route('index.rail') }}">Rail</a></li>
						<li><a href="{{ route('warehousecontroller.index') }}">Warehouse</a></li>
					</ul>
				</li>
				<li>
					<a href="{{ route('index.forCarriers') }}">FOR CARRIERS</a>
				</li>
				<li class="dropdown">
					<a data-toggle="dropdown" class="dropdown-toggle border-hover-color3" href="{{ route('index.aboutUs') }}">ABOUT US <i class="fa fa-angle-down"></i></a>
					<ul class="dropdown-menu">
						<li><a href="{{ route('index.history') }}">History</a></li>
						<li><a href="{{ route('index.filosophy') }}">Philosophy</a></li>
					</ul>
				</li>

				<li><a href="{{ route('contactController.contact') }}">CONTACT</a></li>
				<li><a class="btn_header_search" href="#"><i class="fa fa-search"></i></a></li>
			</ul>
			<div class="search-form-modal transition">
				<form class="navbar-form header_search_form"><i class="fa fa-times search-form_close"></i> <div class="form-group"> <input type="text" class="form-control" placeholder="Search"> </div> <button type="submit" class="btn btn_search customBgColor">Search</button> </form>
			</div>
		</nav>
		<a id="menu-close" href="#"><i class="fa fa-times"></i></a>
	</div>
</header>