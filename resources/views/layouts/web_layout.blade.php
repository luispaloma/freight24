<!DOCTYPE html>
<html lang="en">
@include('layouts.head', ['title' => $title])
<body data-scrolling-animations="true">
	<div class="sp-body">
		@include('layouts.header')
		@yield('content')
		@include('layouts.footer')
	</div>
</body>
</html>