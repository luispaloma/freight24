<footer>
	<div class="color-part2"></div>
	<div class="color-part"></div>
	<div class="container-fluid">
		<div class="row block-content">
			<div class="col-xs-8 col-sm-4 wow zoomIn" data-wow-delay="0.3s">
				<h4>ABOUT US</h4>
				<p>We connect leaders from various fields of logistics on the global market.</p>
				<div class="footer-icons">
					<a href="#"><i class="fa fa-facebook-square fa-2x"></i></a>
					<a href="#"><i class="fa fa-google-plus-square fa-2x"></i></a>
					<a href="#"><i class="fa fa-twitter-square fa-2x"></i></a>
					<a href="#"><i class="fa fa-pinterest-square fa-2x"></i></a>
					<a href="#"><i class="fa fa-vimeo-square fa-2x"></i></a>
				</div>
				<a href="#" class="btn btn-lg btn-danger">GET QUOTE</a>
			</div>
			<div class="col-xs-4 col-sm-2 wow zoomIn" data-wow-delay="0.3s">
				<h4>CATEGORIES</h4>
				<nav>
					<a href="{{ route('lclcontroller.index') }}">Sea Freight</a>
					<a href="{{ route('index.road_ltl') }}">Road Freight</a>
					<a href="{{ route('index.air') }}">Air Freight</a>
					<a href="{{ route('index.rail') }}">Rail Freight</a>
					<a href="">Breakbulk</a>
					<a href="#">Warehouse</a>
				</nav>
			</div>
			<div class="col-xs-6 col-sm-2 wow zoomIn" data-wow-delay="0.3s">
				<h4>MAIN LINKS</h4>
				<nav>
					<a href="01_home.html">Home</a>
					<a href="06_services.html">Our Services</a>
					<a href="04_about.html">About Us</a>
					<a href="07_services.html">News</a>
					<a href="12_contact.html">Contact</a>
				</nav>
			</div>
			<div class="col-xs-6 col-sm-4 wow zoomIn" data-wow-delay="0.3s">
				<h4>CONTACT INFO</h4>
				Satisfaction is the key of success. Don’t hestitate to contact us.
				<div class="contact-info">
					<span><i class="fa fa-location-arrow"></i><strong>I-Freight24 GmbH</strong><br>Postfach 1515, 4133 Pratteln, Switzerland</span>
					<span><i class="fa fa-envelope"></i>info@i-freight24.com</span>
				</div>
			</div>
		</div>
		<div class="copy text-right"><a id="to-top" href="#this-is-top"><i class="fa fa-chevron-up"></i></a> &copy; 2017 I-Freight 24 All rights reserved.</div>
	</div>
</footer>

<!--Main-->
<script src="{{ asset('public/css/front/js/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('public/css/front/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('public/css/front/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/css/front/js/modernizr.custom.js') }}"></script>

<script src="{{asset('public/css/front/assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('public/css/front/js/waypoints.min.js')}}"></script>
<script src="{{asset('public/css/front/js/jquery.easypiechart.min.js')}}"></script>
<!-- Loader -->
<script src="{{asset('public/css/front/assets/loader/js/classie.js')}}"></script>
<script src="{{asset('public/css/front/assets/loader/js/pathLoader.js')}}"></script>
<script src="{{asset('public/css/front/assets/loader/js/main.js')}}"></script>
<script src="{{asset('public/css/front/js/classie.js')}}"></script>
<!--Switcher-->
<script src="{{asset('public/css/front/assets/switcher/js/switcher.js')}}"></script>
<!--Owl Carousel-->
<script src="{{asset('public/css/front/assets/owl-carousel/owl.carousel.min.js')}}"></script>
<!-- SCRIPTS -->
<script type="text/javascript" src="{{asset('public/css/front/assets/isotope/jquery.isotope.min.js')}}"></script>
<!--Theme-->
<script src="{{asset('public/css/front/js/jquery.smooth-scroll.js')}}"></script>
<script src="{{asset('public/css/front/js/wow.min.js')}}"></script>
<script src="{{asset('public/css/front/js/jquery.placeholder.min.js')}}"></script>
<script src="{{asset('public/css/front/js/smoothscroll.min.js')}}"></script>
<script src="{{asset('public/css/front/js/theme.js')}}"></script>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
@section('custom_scripts')
@show