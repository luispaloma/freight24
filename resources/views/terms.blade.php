@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'Terms and Conditions'])

<div class="container-fluid inner-offset">
	<div class="hgroup text-center wow zoomIn" data-wow-delay="0.3s">
		<h2>Terms and Conditions</h2>
		<h1>i-FREIGHT 24 - THE RIGHT CHOICE</h1>
	</div>

					<div class="tab-content inner-offset wow fadeIn" data-wow-delay="0.3s">
						<div class="tab-pane active" id="tab1">
							<div class="row">
								<div class="col-sm-5">
									<img class="full-width" src="media/images/1.jpg" alt="Img">
								</div>
								<div class="col-sm-7">
									<p>By using www.i-freight24.com you have agreed with all the terms specified in this document. If you are not agreed with any term specified in the document, you are not authorized to have access to this site. We reserve the right to update, modify or change some or all the terms if needed without warning or prior notice. <br><br>
	By using i-Freight24 you don't get license or other rights of property on prov<br><br>ided information or services. You can use i-Freight24 for personal use only without having rights of property or rights on services of i-Freight24 GmbH. <br><br>
	i-Freight24 GmbH does not guarantee your satisfaction of using results got with the help of our service. <br><br>
	i-Freight24 GmbH doesn't guarantee the accuracy and reliability of results got at using our service as well as safety, accuracy, timeliness and continuity of our site. i-Freight24 GmbH doesn’t guarantee completeness and integrity of a service or material got through the site or such that satisfies your expectations including product, services and materials got or acquired through this site. i-Freight24 GmbH assumes no responsibility for a provided information. <br><br>
	Business relations or other relationships with i-Freight24 GmbH advertising companies are between you and the advertising companies. Under no circumstances i-Freight24 GmbH will be liable in any way for any losses experienced by you in the result of your supporting those business relations including losses experienced as the result of advertiser's posting at our site. <br><br>
	i-Freight24 GmbH has no control over the content and services linked from our server. We assume no responsibility for advertisement, content, products, services or other materials are available through those sites and sources. <br><br>
	i-Freight24 GmbH assumes no responsibility for any loss or damage caused by computer virus got at loading information or content sent by a third party.<br><br>
	Keeping your private data including user id and password is up to you. You can't share or disclose your password to third parties. We reserve the right to terminate your account without refund if access of third parties to your account is detected. <br><br>
	You are to accept the Rules of i-Freight24 GmbH to use it. <br><br>
	i-Freight24 GmbH assumes the right to limit or suspend access to some users upon violation of these or other rules published on site.
	</p>

	<h2>Scope of our Service</h2>
	<p>
		Through the website www.i-freight24.com we provide an online platform through which all types of shipment by seafreight, airfreight, trucking, rail, breakbulk and also warehouse can be advertise for pricing and through which shipping service providers can make their bits. <br><br>
	By confirming a bit on your request you enter into a direct (legally binding) contractual relationship with the shipping service provider at which you gave the confirmation.  <br><br>
	From the point at which you confirmed the bit we act solely as an intermediary between you and the carriers with our platform, transmitting the details of your company to the relevant shipping service provider and sending you a confirmation e-mail with the details of the shipping service provider. <br><br>
	When rendering our services, the information that we disclose is based on the information provided to us by shipping service providers. As such, the shipping service providers are given access to an extranet through which they are fully responsible for updating all rates, availability and other information which is displayed on our website. <br><br>
	Although we will use reasonable skill and care in performing our services we will not verify if, and cannot guarantee that, all information is accurate, complete or correct, nor can we be held responsible for any errors.<br><br>
	Each shipping service provider remains responsible at all times for the accuracy, completeness and correctness of the (Rate descriptive) information (including the rates and availability) displayed on our website. Our website does not constitute and should not be regarded as a recommendation or endorsement of the quality, service level of qualification of any service made available. <br><br>
	You are not allowed to re-sell, deep-link, use, copy, monitor (e.g. spider, scrape), display, download or reproduce any content or information, software, products or services available on our website. <br><br>
	</p>
	<h2>Prices</h2>
	<p>
		The prices on our site are highly competitive. All prices on the SeaRates.com website are per container are displayed including VAT and all other taxes (subject to change of such taxes), unless stated differently on our website or the confirmation email. <br><br>
	Sometimes cheaper prices are available on our website for a certain shipment, but these prices can vary from the freight forwarders. We are, however, endeavoring to provide our customers with prices on the same basis level, so that all freight forwarders place their offers on the same Level.<br><br>
	</p>
	<h2>Free of charge</h2>
	<p>
		Our service includes an annual fee. Various packages are available, so you can choose your own needs, which is appropriate for you. We will not charge any fees on the quotes of the freight forwarders.
	</p>
	<h2>Refund Policy</h2>
	<p>Services purchased on www.i-freight24.com will not be refunded, even if they are not used or the account is canceled at the request of the account owner. If you have any questions about our service, please do not hesitate to contact us by e-mail.</p>
	<h2>Charges and Claims</h2>
	<p>The costs for the first registration have to be paid in advance for the whole year. After this, the account will be opened after checking all the data and you will be able to pay the fee on a monthly basis from the second year. All applications for new registrations, are checked by our technical and legal department for correctness. Enterprises must meet a certain standard in order to be accepted by us. If you have a fraudulent claim, we will forward this directly to our legal department, which will take action.</p>
								</div>
							</div>
						</div>
					</div>
</div>

@endsection