@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'Air Freight'])
<style type="text/css">
			.formText {
				height: 56px;
				padding-top: 11px;
				margin-bottom: 0!important;
			}
			button#dropdownMenu1 {
				margin-bottom: 10px;
			}
			</style>
<img src="{{ asset('public/css/front/img/airFreight.png') }}" style="width: 100%" />
<div class="container-fluid block-content">
	<div class="row main-grid">
		@include('layouts.form_sidebar_menu')
		<div class="col-sm-3"></div>
		<div class="col-sm-9">
			<div class="row">
				<div id="success"></div>
				<form novalidate id="contactForm" class="reply-form">
					<div class="col-xs-6">
						<div class="form-group">
							<label class="formText" for="commodity">Commodity</label>
							<input type="email" class="form-control" id="commodity" placeholder="Enter Commodity">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">HS Code</label>
							<input type="email" class="form-control" id="HS" placeholder="Enter HS Code">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Type of package</label>
							<input type="email" class="form-control" id="HS" placeholder="">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Number of package</label>
							<input type="email" class="form-control" id="HS" placeholder="Enter number">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Dimension</label>
							<input type="email" class="form-control" id="HS" placeholder="ej: 120 x 120 x 190">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Weight of cargo</label>
							<input type="email" class="form-control" id="HS" placeholder="ej: 500kg">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Stackable</label>
							<select class="form-control">
								<option>No</option> <option>Yes </option>
							</select>
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Dangerous goods</label>
							<select class="form-control">
								<option>No</option> <option>Yes </option>
							</select>
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Total weight</label>
							<input type="email" class="form-control" id="HS" placeholder="Enter weight">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Total volume</label>
							<input type="email" class="form-control" id="HS" placeholder="ej: 8.901 cbm">
						</div>
					</div>
					<div class="col-xs-6">
						<div class="form-group">
							<label class="formText" for="HS">Trade</label>
							<select class="form-control">
								<option>Export</option> <option>Import</option> <option>Crosstrade</option>
							</select>
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Cargo ready date</label>
							<input type="text" class="form-control" id="HS" placeholder="Select date">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Pick up date</label>
							<input type="text" class="form-control" id="HS" placeholder="Select date">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Total value of cargo</label>
							<input type="text" class="form-control" id="HS" placeholder="Enter cargo value">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Do you need Transport Insurance?</label>
							<select class="form-control">
								<option>No</option> <option>Yes </option>
							</select>
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Do you have regular shipments?</label>
							<select class="form-control">
								<option>No</option> <option>Yes </option>
							</select>
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Total value of cargo</label>
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Shipping details / Information ">
						</div>
						<a type="submit" class="btn btn-danger" href="price-select.php">CONTINUE</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection