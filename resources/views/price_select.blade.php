@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'For Carriers'])

<div class="container-fluid inner-offset">
	<div class="hgroup text-center wow zoomIn" data-wow-delay="0.3s">
		<h2>History</h2>
		<h1>i-FREIGHT 24 - THE RIGHT CHOICE</h1>
	</div>

	<div class="container-fluid block-content">
		<div class="row main-grid">
			<div class="hgroup text-center wow zoomInUp" data-wow-delay="0.3s">
			<h1>SELECT YOUR PLAN</h1>
		</div>
			<div class="row">
					<div class="col-sm-12">
						<div class="col-sm-3">
							<div class="price-table">
							<h1>SMART</h1>
							<p>1 log in for one field (like only trucking LTL/FTL or seafreight FCL/LCL)</p>
							<h1>9.90 $ per month</h1>
							<button type="submit" class="btn btn-danger">Select</button>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="price-table">
							<h1>BUSINESS I</h1>
							<p>1 log in for all sectors in I-freight24 (incl. trucking, sea, air, bulk, warehouse, rail) </p>
							<h1>19.90 $ per month</h1>
							<button type="submit" class="btn btn-danger">Select</button>
						</div>
						</div>
						<div class="col-sm-3">
							<div class="price-table">
							<h1>BUSINESS II</h1>
							<p>5 log in for all sectors in I-freight24 (incl. trucking, sea, air, bulk, warehouse, rail)</p>
							<h1>29.90 $ per month</h1>
							<button type="submit" class="btn btn-danger">Select</button>
						</div>
						</div>
						<div class="col-sm-3">
							<div class="price-table">
						 <h1>PREMIUM</h1>
							<p>20 log in for all sectors in I-freight24 (incl. trucking, sea, air, bulk, warehouse, rail)</p>
							<h1>49.90 $ per month</h1>
							<button type="submit" class="btn btn-danger">Select</button>
						</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>

@endsection