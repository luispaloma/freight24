@extends('Admin.layout')

@section('title')
    i-Freight 24 CMS Dashboard
@stop


@section('content')

<!-- Content begins -->
<div id="content">
    <div class="contentTop">
        <span class="pageTitle"><span class="icon-screen"></span>Dashboard</span>
        <ul class="quickStats">
            <li>
                <a href="" class="redImg"><img src="{{ asset('public/images/icons/quickstats/user.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">4658</strong><span>users</span></div>
            </li>
            <li>
                <a href="" class="greenImg"><img src="{{ asset('public/images/icons/quickstats/money.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">1289</strong><span>orders</span></div>
            </li>
        </ul>
    </div>

    <!-- Breadcrumbs line -->
    <div class="breadLine">
        <div class="bc">
            <ul id="breadcrumbs" class="breadcrumbs">
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">Home</a>
                    <ul>
                        <li><a href="company.html" title="">General elements</a></li>
                        <li><a href="ui_icons.html" title="">Icons</a></li>
                         <li><a href="ui_buttons.html" title="">Button sets</a></li>
                        <li><a href="ui_custom.html" title="">Custom elements</a></li>
                    </ul>
                </li>
                <li class="current"><a href="ui_grid.html" title="">Menu</a></li>
            </ul>
        </div>

        <div class="breadLinks">
            <ul>
                <li><a href="#" title=""><i class="icos-list"></i><span>Orders</span> <strong>(+58)</strong></a></li>
                <li><a href="#" title=""><i class="icos-check"></i><span>Tasks</span> <strong>(+12)</strong></a></li>
                <li class="has">
                    <a title="">
                        <i class="icos-money3"></i>
                        <span>Invoices</span>
                        <span><img src="{{ asset('public/images/elements/control/hasddArrow.png') }}" alt="" /></span>
                    </a>
                    <ul>
                        <li><a href="#" title=""><span class="icos-add"></span>New invoice</a></li>
                        <li><a href="#" title=""><span class="icos-archive"></span>History</a></li>
                        <li><a href="#" title=""><span class="icos-printer"></span>Print invoices</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <!-- Main content -->
    <div class="wrapper">
        <ul class="middleNavR">
            <li><a href="#" title="Add a new quote" class="tipN"><img src="{{ asset('public/images/icons/middlenav/create.png') }}" alt="" /></a></li>
            <li><a href="#" title="Upload files" class="tipN"><img src="{{ asset('public/images/icons/middlenav/upload.png') }}" alt="" /></a></li>
            <li><a href="#" title="Messages" class="tipN"><img src="{{ asset('public/images/icons/middlenav/dialogs.png') }}" alt="" /></a><strong>8</strong></li>
            <li><a href="#" title="Check statistics" class="tipN"><img src="{{ asset('public/images/icons/middlenav/stats.png') }}" alt="" /></a></li>
        </ul>

    	<!-- Chart -->
        <div class="widget chartWrapper">
            <div class="whead"><h6>Charts</h6>
                <div class="titleOpt">
                    <a href="#" data-toggle="dropdown"><span class="icos-cog3"></span></a>
                    <ul class="dropdown-menu pull-right">
                            <li><a href="#"><span class="icos-add"></span>Add</a></li>
                            <li><a href="#"><span class="icos-trash"></span>Remove</a></li>
                            <li><a href="#" class=""><span class="icos-pencil"></span>Edit</a></li>
                            <li><a href="#" class=""><span class="icos-heart"></span>Do whatever you like</a></li>
                    </ul>
                </div>
            </div>
            <div class="body"><div class="chart"></div></div>
        </div>


    	<!-- 6 + 6 -->
        <div class="fluid">
            <div class="grid6">
                <!-- Search widget -->
                <div class="searchLine">
                    <form action="">
                        <input type="text" name="search" class="ac" placeholder="Enter search text..." />
                       <button type="submit" name="find" value=""><span class="icos-search"></span></button>
                    </form>
                </div>
            </div>
            <!-- Calendar -->
            <div class="widget grid6">
                <div class="whead"><h6>Calendar</h6></div>
                <div id="calendar"></div>
            </div>
        </div>

        <div class="widget">
        <div class="whead"><h6>Companies</h6></div>
        <div id="dyn" class="hiddenpars">
            <a class="tOptions" title="Options"></a>
            <table cellpadding="0" cellspacing="0" border="0" class="dTable" id="dynamic">
            <thead>
            <tr>
            <th>Name<span class="sorting" style="display: block;"></span></th>
            <th>Freight type</th>
            <th>Shipping Specialty</th>
            <th>Date</th>
            </tr>
            </thead>
            <tbody>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
           <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
           <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
           <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            <tr class="gradeX">
            <td>Jhon Doe</td>
            <td>Sea Freight</td>
            <td>500kg clothes</td>
            <td class="center">07/17/2017</td>
            </tr>
            </tbody>
            </table>
        </div>
        </div>



            <!-- Media table -->
          <div class="widget check grid6">
            <div class="whead">
                <span class="titleIcon"><input type="checkbox" id="titleCheck" name="titleCheck" /></span>
                <h6>Users</h6>
            </div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tDefault checkAll tMedia" id="checkAll">
                <thead>
                    <tr>
                        <td><img src="{{ asset('public/images/elements/other/tableArrows.png') }}" alt="" /></td>
                        <td width="50">Image</td>
                        <td class="sortCol"><div>Name<span></span></div></td>
                        <td width="130" class="sortCol"><div>Registration Date<span></span></div></td>
                        <td width="100">Actions</td>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="6">
                            <div class="itemActions">
                                <label>Apply action:</label>
                                <select class="styled">
                                    <option value="">Select action...</option>
                                    <option value="Edit">Edit</option>
                                    <option value="Delete">Delete</option>
                                    <option value="Move">Move somewhere</option>
                                </select>
                            </div>
                            <div class="tPages">
                                <ul class="pages">
                                    <li class="prev"><a href="#" title=""><span class="icon-arrow-14"></span></a></li>
                                    <li><a href="#" title="" class="active">1</a></li>
                                    <li><a href="#" title="">2</a></li>
                                    <li><a href="#" title="">3</a></li>
                                    <li><a href="#" title="">4</a></li>
                                    <li>...</li>
                                    <li><a href="#" title="">20</a></li>
                                    <li class="next"><a href="#" title=""><span class="icon-arrow-17"></span></a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td><input type="checkbox" name="checkRow" /></td>
                        <td><a href="images/big.png" title="" class="lightbox"><img src="{{ asset('public/images/live/face3.png') }}" alt="" /></a></td>
                        <td class="textL"><a href="#" title="">Name</a></td>
                        <td>Feb 12, 2012. 12:28</td>
                        <td class="tableActs">
                            <a href="#" class="tablectrl_small bDefault tipS" title="Edit"><span class="iconb" data-icon="&#xe1db;"></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" title="Options"><span class="iconb" data-icon="&#xe1f7;"></span></a>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="checkRow" /></td>
                        <td><a href="images/big.png" title="" class="lightbox"><img src="{{ asset('public/images/live/face7.png') }}" alt="" /></a></td>
                        <td class="textL"><a href="#" title="">Name</a></td>
                        <td>Feb 12, 2012. 12:28</td>
                        <td class="tableActs">
                            <a href="#" class="tablectrl_small bDefault tipS" title="Edit"><span class="iconb" data-icon="&#xe1db;"></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" title="Options"><span class="iconb" data-icon="&#xe1f7;"></span></a>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="checkRow" /></td>
                        <td><a href="images/big.png" title="" class="lightbox"><img src="{{ asset('public/images/live/face6.png') }}" alt="" /></a></td>
                        <td class="textL"><a href="#" title="">Name</a></td>
                        <td>Feb 12, 2012. 12:28</td>
                        <td class="tableActs">
                            <a href="#" class="tablectrl_small bDefault tipS" title="Edit"><span class="iconb" data-icon="&#xe1db;"></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" title="Options"><span class="iconb" data-icon="&#xe1f7;"></span></a>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="checkRow" /></td>
                        <td><a href="images/big.png" title="" class="lightbox"><img src="{{ asset('public/images/live/face5.png') }}" alt="" /></a></td>
                        <td class="textL"><a href="#" title="">Name</a></td>
                        <td>Feb 12, 2012. 12:28</td>
                        <td class="tableActs">
                            <a href="#" class="tablectrl_small bDefault tipS" title="Edit"><span class="iconb" data-icon="&#xe1db;"></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" title="Options"><span class="iconb" data-icon="&#xe1f7;"></span></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>




    </div>
    <!-- Main content ends -->
@endsection