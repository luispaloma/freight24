@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'Login'])

<div class="row login_form">
	<h1 style="text-align:center">Please, Enter your information</h1>
	<div class="col-md-5 col-md-offset-2">
		<section class="container">
			<div class="login">
				<form method="post" action="{{ route('login') }}">
					{{ csrf_field() }}
					@if (count($errors) > 0)
					<div class="row">
						<div class="col-sm-12">
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						</div>
					</div>
					@endif
					<p>
						<input  class="login-form-input"  type="text" name="email" value="" placeholder=" Username or Email">
					</p>
					<p>
						<input  class="login-form-input"  type="password" name="password" value="" placeholder=" Password">
					</p>
					<p class="remember_me">
						<label>
							<input type="checkbox" name="remember_me" id="remember_me">
							Remember me on this computer
						</label>
					</p>
					<p class="submit"><input class="btn btn-success" type="submit" name="commit" value="Login"></p>
				</form>
			</div>
			<div class="login-help">
				<p>Forgot your password? <a href="index.html">Click here to reset it</a>.</p>
			</div>
		</section>
	</div>
</div>
@endsection