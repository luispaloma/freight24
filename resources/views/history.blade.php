@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'History'])

<div class="container-fluid inner-offset">
<div class="hgroup text-center wow zoomIn" data-wow-delay="0.3s">
	<h2>History</h2>
	<h1>i-FREIGHT 24 - THE RIGHT CHOICE</h1>
</div>


<div class="tab-content inner-offset wow fadeIn" data-wow-delay="0.3s">
	<div class="tab-pane active" id="tab1">
		<div class="row">
			<div class="col-sm-5">
				<img class="full-width" src="{{asset('public/css/front/media/images/1.jpg')}}" alt="Img">
			</div>
			<div class="col-sm-7">
				<p>I-Freight24.com and I-Freight24 GmbH are the leader in quotation of transports worldwide. I-Freight24.com is one of the largest Freight Quotation and Exchange Companies based in Switzerland. We do not provide a direct or automatic quotation service but we can help you to find qualified partnes that will assist you to get the best prices for your transport. In other words you upload your freight request in our markeplace and receive shortly quotes from prooved carriers which want to organize the transport for you. Start with the creation of an i-Freight24 Account and list your shipments on the marketplace. Make sure to be as detailed as possible and provide all necessary information. Accept the most attractive bid and secure  a bigger win for yourself. Here you are - it's done.</p>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid block-content percent-blocks" data-waypoint-scroll="true">
	<div class="row stats">
		<div class="col-sm-6 col-md-3 col-lg-3"> <div class="chart" data-percent="230"> <span><i class="fa fa-folder-open"></i></span> <span class="percent"></span>Projects Done </div> </div> <div class="col-sm-6 col-md-3 col-lg-3">
		<div class="chart" data-percent="68"> <span><i class="fa fa-users"></i></span> <span class="percent"></span>Clients Worldwide </div> </div>
		<div class="col-sm-6 col-md-3 col-lg-3">
			<div class="chart" data-percent="147"> <span><i class="fa fa-truck"></i></span> <span class="percent"></span>Official Branches </div> </div>
			<div class="col-sm-6 col-md-3 col-lg-3"> <div class="chart" data-percent="105"> <span><i class="fa fa-male"></i></span> <span class="percent"></span>Satisfied Clients </div> </div> </div>
		</div>
		<div class="container-fluid partners block-content">
			<div class="hgroup title-space wow fadeInLeft" data-wow-delay="0.3s">
				<h1>TRUSTED partners</h1>
				<h2>Lorem ipsum dolor sit amet consectetur</h2>
			</div>
			<div id="partners" class="owl-carousel enable-owl-carousel" data-pagination="false" data-navigation="true" data-min450="2" data-min600="2" data-min768="4">
				<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="{{asset('public/css/front/media/partners/1.png')}}" alt="Img"></a></div>
				<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="{{asset('public/css/front/media/partners/2.png')}}" alt="Img"></a></div>
				<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="{{asset('public/css/front/media/partners/3.png')}}" alt="Img"></a></div>
				<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="{{asset('public/css/front/media/partners/4.png')}}" alt="Img"></a></div>
				<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="{{asset('public/css/front/media/partners/1.png')}}" alt="Img"></a></div>
				<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="{{asset('public/css/front/media/partners/2.png')}}" alt="Img"></a></div>
			</div>
		</div>

@endsection