@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'Air Freight'])

<iframe class="we-onmap wow fadeInUp" data-wow-delay="0.3s"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3000709.798888579!2d-78.0527349290536!3d42.73799782521889!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4ccc4bf0f123a5a9%3A0xddcfc6c1de189567!2sNueva+York%2C+EE.+UU.!5e0!3m2!1ses!2sco!4v1471233362604" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

<div class="container-fluid block-content">
	<div class="row main-grid">
		<div class="col-sm-4">
			<h4>Head Office</h4>
			<p>Everyday is a new day for us and we work really hard to
				satisfy our customers everywhere.</p>
			<div class="adress-details wow fadeInLeft" data-wow-delay="0.3s">
				<div>
					<span><i class="fa fa-location-arrow"></i></span>
					<div><strong>i-Freight 24</strong><br>3608 NewHill Station Ave CA,  Newyork 33102 </div>
				</div>
				<div>
					<span><i class="fa fa-envelope"></i></span>
					<div> info@i-freight24.com</div>
				</div>
				<div>
					<span><i class="fa fa-clock-o"></i></span>
					<div>24h service</div>
				</div>
			</div>
			<br><br><hr><br>
		</div>
		<div class="col-sm-8 wow fadeInRight" data-wow-delay="0.3s">
			<h4>Contact Us</h4>
			<p>If you need help or you have a question you can contact us as here:</p>
			<div id="success"></div>
			<form action="{{ route('contactController.SendMessage') }}" novalidate id="contactForm" class="reply-form form-inline" method="POST">
				{{ csrf_field() }}
				@if (count($errors) > 0)
				<div class="row">
					<div class="col-sm-12">
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					</div>
				</div>
				@endif
				<div class="row form-elem">
					<div class="col-sm-6 form-elem">
						<div class="default-inp form-elem">
							<i class="fa fa-user"></i>
							<input type="text" name="user_name" id="user_name" placeholder="Full Name" required="required" value="{{ old('user_name') }}">
						</div>
						<div class="default-inp form-elem">
							<i class="fa fa-envelope"></i>
							<input type="text" name="user_email" id="user_email" placeholder="Email Address" required="required" value="{{ old('user_email') }}">
						</div>
					</div>
					<div class="col-sm-6 form-elem">
						<div class="default-inp form-elem">
							<i class="fa fa-user"></i>
							<input type="text" name="user_lastname" id="user_lastname" placeholder="Last Name" required="" value="{{ old('user_lastname') }}">
						</div>
						<div class="default-inp form-elem">
							<i class="fa fa-phone"></i>
							<input type="text" name="user_phone" id="user_phone" placeholder="Phone No." required="" value="{{ old('user_phone') }}">
						</div>
					</div>
				</div>
				<div class="default-inp form-elem">
					<input type="text" name="user_subject" id="user_subject" placeholder="Subject" required="" value="{{ old('user_subject') }}">
				</div>
				<div class="form-elem default-inp">
					<textarea id="user_message" name="user_message" placeholder="Message" required="">{{ old('user_message') }}</textarea>
				</div>
				<div class="form-elem">
					<button type="submit" class="btn btn-success btn-default">send message</button>
				</div>
			</form>
		</div>
	</div>
</div>


@endsection

@section('custom_scripts')
    <script type="text/javascript">
		$("#contactForm").validate();
    </script>
@endsection