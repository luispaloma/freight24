@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'History'])

<img src="{{ asset('public/css/front/img/roadFreight.png') }}" style="width: 100%" />
<div class="container-fluid block-content">
	<div class="row main-grid">
		<div class="col-sm-3 wow slideInUp" data-wow-delay="0.3s">
			<div class="sidebar-container">
				<div>
					<ul class="styled">
						<li class="active"><a href="05_services.html">SEA FREIGHT</a></li>
						<li><a href="05_road.html">Road transportation</a></li>
						<li><a href="05_air.html">AIR FREIGHT</a></li>
						<li><a href="06_services.html">packaging & storage</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-sm-9">
			<div class="row">
				<div class="col-xs-6 col-sm-6">
					<h4>Place of loading</h4>
					<div>
						<input type="text" class="form-control" placeholder="Enter city" >
						<input type="text" class="form-control" placeholder="Zip Code" >
						location type
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							Dropdown
							<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Separated link</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6"><h4>Place of Discharge</h4>
					<div>
						<input type="text" class="form-control" placeholder="Enter city" >
						<input type="text" class="form-control" placeholder="Zip Code" >
						location type
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							Dropdown
							<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Separated link</a></li>
							</ul>
						</div>
					</div></div>
				</div>
			</div>
			<div class="col-sm-3">

			</div>
			<div class="col-sm-9">

				<div class="row">
					<div id="success"></div>
					<form novalidate id="contactForm" class="reply-form">
						<div class="col-xs-3">
							<p>Type of truck</p>
							<p>Total weight of cargo</p>
							<p>Total volume of cargo</p>
							<p>IMO Class</p>
							<p>UN Number</p>
						</div>
						<div class="col-xs-3">
							<div class="dropdown">
								<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								Dropdown
								<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Automatic Calculation">
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Automatic Calculation">
							<div class="dropdown">
								<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								Dropdown
								<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Enter Number">
						</div>
						<div class="col-xs-3">
							<p>Package Group</p>
							<p>Trade</p>
							<p>Commodity</p>
							<p>HS Code</p>
							<p>Total Value of cargo</p>
							<p>Do you need Transport Insurance</p>
							<p>Do you have regular shipments</p>
							<p>Shipping details / Information</p>
						</div>
						<div class="col-xs-3">
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Enter Number">
							<div class="dropdown">
								<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								Dropdown
								<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Enter cargo details">
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Enter Hs Code">
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Enter Hs Code">
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Do you need Transport Insurance">
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Do you have regular shipments">
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Shipping details / Information	">
						</div>
						<div class="col-xs-6 col-xs-offset-6">
							<button type="submit" class="btn btn-danger">CONTINUE</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection