@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'History'])
<style type="text/css">
    ul.leadsCycleContainer_exchange {
list-style: none;
}
span.last1 {
font-size: 10px;
color: gray;
}
.direction-text {
color: black;
}
.media-left.media-middle {
width: 18%;
}
</style>
<div class="container-fluid inner-offset">
	<div class="container filter_controls">
		<div class="row shipping-box">
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<img src="{{asset('public/css/front/img/less.png')}}">
						<h3 class="mt10">Less container load</h3>
						<p>
							Submit a form for cargo volume from 1 cubic meter and up to 20 m³. LCL shipment is the most economical way to ship small cargo.
						</p>
					</div>
					<div class="panel-footer br-n no-padding">
						<div class="block__roll">
							<ul class="leadsCycleContainer_exchange">
								<li style="height: 33px; opacity: 1; list-style: none;">
									<a class="msidebar" target="_blank" href="#">
										<div class="media">
											<div class="media-left media-middle">
												<span class="date-it">
													<span class="last1">25 min</span>
												</span>
											</div>
											<div class="media-body">
												<i class="flag-icon flag-icon-us"></i>
												<span class="direction-text">83-38 Woodhaven Blvd</span>
												<i class="flag-icon flag-icon-co"></i>
												<span class="direction-text">Cartagena</span></div>
											</div>
										</a>
									</li>
									<li style="height: 33px; opacity: 1; list-style: none;">
										<a class="msidebar" target="_blank" href="/shipping/list/circuito_de_las_universidad_60_centro_60950_lzaro_crdenas_mich_mexico_mx/caldera_provincia_de_puntarenas_espritu_santo_costa_rica_cr_1">
											<div class="media"><div class="media-left media-middle">
												<span class="date-it">
													<span class="last1">1 hour</span>
												</span>
											</div>
											<div class="media-body">
												<i class="flag-icon flag-icon-mx"></i>
												<span class="direction-text">Circuito De Las Universidad 60</span>
												<i class="flag-icon flag-icon-cr"></i>
												<span class="direction-text">Caldera</span>
											</div>
										</div>
									</a>
								</li>
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/620_8th_ave__1_new_york_ny_10018_usa_us/cairo_cairo_governorate_egypt_eg_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">1 hour</span></span></div><div class="media-body"><i class="flag-icon flag-icon-us"></i> <span class="direction-text">620 8th Ave # 1</span><i class="flag-icon flag-icon-eg"></i> <span class="direction-text">Cairo</span></div></div></a></li>
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/agadir_ma/sausalito_us_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">2 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-ma"></i> <span class="direction-text">Agadir</span><i class="flag-icon flag-icon-us"></i> <span class="direction-text">Sausalito</span></div></div></a></li>
								<li style="height: 33px; opacity: 1;list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/santo_domingo_do/santo_domingo_do_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">5 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-do"></i> <span class="direction-text">Santo Domingo</span><i class="flag-icon flag-icon-do"></i> <span class="direction-text">Santo Domingo</span></div></div></a></li>
							</ul>                    </div>
						</div>
					</div>
					<!--div class="text-center">
					<a href="/shipping/list/">All Shipping Leads</a>
				</div-->
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<img src="{{asset('public/css/front/img/fullContainer.png')}}">
						<h3 class="mt10">Full container load</h3>
						<p>
							Transportation in ocean containers (FCL). Whole container is intended for one consignee. Submit rate request form for one type of container.
						</p>
					</div>
					<div class="panel-footer br-n no-padding">
						<div class="block__roll">
							<ul class="leadsCycleContainer_exchange">
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/bandar_abbas_port_ir/ho_chi_minh_city_port_vn_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">59 min</span></span></div><div class="media-body"><i class="flag-icon flag-icon-ir"></i> <span class="direction-text">Bandar Abbas</span><i class="flag-icon flag-icon-vn"></i> <span class="direction-text">Ho Chi Minh City</span></div></div></a></li>
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/altamira_port_mx/hamburg_port_de_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">1 hour</span></span></div><div class="media-body"><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Altamira</span><i class="flag-icon flag-icon-de"></i> <span class="direction-text">Hamburg</span></div></div></a></li>
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/houston_port_us/zeebgrugge_port_be_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">1 hour</span></span></div><div class="media-body"><i class="flag-icon flag-icon-us"></i> <span class="direction-text">Houston</span><i class="flag-icon flag-icon-be"></i> <span class="direction-text">Zeebgrugge</span></div></div></a></li>
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/altamira_port_mx/hamburg_port_de_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">2 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Altamira</span><i class="flag-icon flag-icon-de"></i> <span class="direction-text">Hamburg</span></div></div></a></li>
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/prince_rupert_port_ca/mumbai_port_in_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">2 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-ca"></i> <span class="direction-text">Prince Rupert</span><i class="flag-icon flag-icon-in"></i> <span class="direction-text">Mumbai</span></div></div></a></li>
							</ul>                    </div>
						</div>
					</div>
					<!--div class="text-center">
					<a href="/shipping/list/">All Shipping Leads</a>
				</div-->
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<img src="{{asset('public/css/front/img/in-Bulk.png')}}" alt="">
						<h3 class="mt10">In Bulk</h3>
						<p>
							Break bulk cargo, Bulk Cargoes, Project cargo and Heavy lift, military equipment or almost any other oversized or overweight cargo.
						</p>
					</div>
					<div class="panel-footer br-n no-padding">
						<div class="block__roll">
							<ul class="leadsCycleContainer_exchange">
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="#"><div class="media"><div class="media-left media-middle"><span class="last1">3 days</span></div><div class="media-body"><i class="flag-icon flag-icon-ua"></i> <span class="direction-text">Yuzhnyy</span><i class="flag-icon flag-icon-my"></i> <span class="direction-text">Port Kelang</span></div></div></a></li>
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/novorossiysk_port_ru/kaohsiung_port_tw_1"><div class="media"><div class="media-left media-middle"><span class="last1">3 days</span></div><div class="media-body"><i class="flag-icon flag-icon-ru"></i> <span class="direction-text">Novorossiysk</span><i class="flag-icon flag-icon-tw"></i> <span class="direction-text">Kaohsiung</span></div></div></a></li>
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/altamira_port_mx/altamira_port_mx_1"><div class="media"><div class="media-left media-middle"><span class="last1"><span class="last1">23 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Altamira</span><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Altamira</span></div></div></a></li>
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/manzanillo_port_mx/yokohama_port_jp_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">23 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Manzanillo</span><i class="flag-icon flag-icon-jp"></i> <span class="direction-text">Yokohama</span></div></div></a></li>
								<li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/manzanillo_port_mx/yokohama_port_jp_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">23 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Manzanillo</span><i class="flag-icon flag-icon-jp"></i> <span class="direction-text">Yokohama</span></div></div></a></li>
							</ul>                    </div>
						</div>
					</div>
					<!--div class="text-center">
					<a href="/shipping/list/">All Shipping Leads</a>
				</div-->
			</div>
		</div>
	</div>
</div>

@endsection