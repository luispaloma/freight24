<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>
		@yield('title')
	</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">

</head>
<body>
	<!-- Top line begins -->
	<div id="top">
		<div class="wrapper">
			<a href="#" title="" class="logo"><img src="{{ asset('public/images/logo.png') }}" alt="" /></a>

			<!-- Right top nav -->
			<div class="topNav">
				<ul class="userNav">
					<li><a href="#" title="" class="screen"></a></li>
					<li><a href="#" title="" class="settings"></a></li>
					<li><a href="{{ route('admin.logout') }}" title="" class="logout"></a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- Top line ends -->
	@if ( !Route::is('admin.index') )
		@include('Admin.sidebar')
	@endif

	@yield('content', 'default')

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

	<script type="text/javascript" src="{{ asset('public/js/plugins/charts/excanvas.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/charts/jquery.flot.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/charts/jquery.flot.orderBars.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/charts/jquery.flot.pie.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/charts/jquery.flot.resize.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/charts/jquery.sparkline.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('public/js/plugins/tables/jquery.dataTables.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/tables/jquery.sortable.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/tables/jquery.resizable.js') }}"></script>

	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.autosize.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.uniform.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.inputlimiter.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.tagsinput.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.maskedinput.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.autotab.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.dualListBox.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.cleditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.ibutton.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.validationEngine-en.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.validationEngine.js') }}"></script>

	<script type="text/javascript" src="{{ asset('public/js/plugins/uploader/plupload.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/uploader/plupload.html4.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/uploader/plupload.html5.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/uploader/jquery.plupload.queue.js') }}"></script>

	<script type="text/javascript" src="{{ asset('public/js/plugins/wizards/jquery.form.wizard.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/wizards/jquery.validate.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/wizards/jquery.form.js') }}"></script>

	<script type="text/javascript" src="{{ asset('public/js/plugins/ui/jquery.collapsible.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/ui/jquery.breadcrumbs.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/ui/jquery.tipsy.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/ui/jquery.progress.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/ui/jquery.timeentry.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/ui/jquery.colorpicker.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/ui/jquery.jgrowl.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/ui/jquery.fancybox.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/ui/jquery.fileTree.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/ui/jquery.sourcerer.js') }}"></script>

	<script type="text/javascript" src="{{ asset('public/js/plugins/others/jquery.fullcalendar.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/others/jquery.elfinder.js') }}"></script>

	<script type="text/javascript" src="{{ asset('public/js/plugins/forms/jquery.mousewheel.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/plugins/ui/jquery.easytabs.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/files/bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/files/login.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/files/functions.js') }}"></script>
</body>
</html>