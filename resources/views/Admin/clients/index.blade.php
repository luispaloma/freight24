@extends('Admin.layout')

@section('title')
    i-Freight 24 CMS Company
@stop


@section('content')

<!-- Content begins -->
<div id="content">
    <div class="contentTop">
        <span class="pageTitle"><span class="icon-link"></span>General form elements</span>
        <ul class="quickStats">
            <li>
                <a href="" class="blueImg"><img src="{{ asset('public/images/icons/quickstats/plus.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">5489</strong><span>visits</span></div>
            </li>
            <li>
                <a href="" class="redImg"><img src="{{ asset('public/images/icons/quickstats/user.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">4658</strong><span>users</span></div>
            </li>
            <li>
                <a href="" class="greenImg"><img src="{{ asset('public/images/icons/quickstats/money.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">1289</strong><span>orders</span></div>
            </li>
        </ul>
    </div>

    <!-- Breadcrumbs line -->
    <div class="breadLine">
        <div class="bc">
            <ul id="breadcrumbs" class="breadcrumbs">
                <li><a href="index.html">Dashboard</a></li>
                <li><a href="forms.html">Forms stuff</a>
                    <ul>
                        <li><a href="form_validation.html" title="">Validation</a></li>
                        <li><a href="form_editor.html" title="">File uploader &amp; WYSIWYG</a></li>
                        <li><a href="form_wizards.html" title="">Form wizards</a></li>
                    </ul>
                </li>
                <li class="current"><a href="forms.html" title="">Inputs &amp; elements</a></li>
            </ul>
        </div>

        <div class="breadLinks">
            <ul>
                <li><a href="#" title=""><i class="icos-list"></i><span>Orders</span> <strong>(+58)</strong></a></li>
                <li><a href="#" title=""><i class="icos-check"></i><span>Tasks</span> <strong>(+12)</strong></a></li>
                <li class="has">
                    <a title="">
                        <i class="icos-money3"></i>
                        <span>Invoices</span>
                        <span><img src="{{ asset('public/images/elements/control/hasddArrow.png') }}" alt="" /></span>
                    </a>
                    <ul>
                        <li><a href="#" title=""><span class="icos-add"></span>New invoice</a></li>
                        <li><a href="#" title=""><span class="icos-archive"></span>History</a></li>
                        <li><a href="#" title=""><span class="icos-printer"></span>Print invoices</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <!-- Main content -->
    <div class="wrapper">
        @include('Admin.clients.middle_nav')

        <h2>TO DO: clients list</h2>
    </div>
</div>
<!-- Content ends -->

@endsection