@extends('Admin.layout')

@section('title')
    i-Freight 24 CMS Company
@stop


@section('content')

<!-- Content begins -->
<div id="content">
    <div class="contentTop">
        <span class="pageTitle"><span class="icon-link"></span>General form elements</span>
        <ul class="quickStats">
            <li>
                <a href="" class="blueImg"><img src="{{ asset('public/images/icons/quickstats/plus.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">5489</strong><span>visits</span></div>
            </li>
            <li>
                <a href="" class="redImg"><img src="{{ asset('public/images/icons/quickstats/user.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">4658</strong><span>users</span></div>
            </li>
            <li>
                <a href="" class="greenImg"><img src="{{ asset('public/images/icons/quickstats/money.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">1289</strong><span>orders</span></div>
            </li>
        </ul>
    </div>

    <!-- Breadcrumbs line -->
    <div class="breadLine">
        <div class="bc">
            <ul id="breadcrumbs" class="breadcrumbs">
                <li><a href="index.html">Dashboard</a></li>
                <li><a href="forms.html">Forms stuff</a>
                    <ul>
                        <li><a href="form_validation.html" title="">Validation</a></li>
                        <li><a href="form_editor.html" title="">File uploader &amp; WYSIWYG</a></li>
                        <li><a href="form_wizards.html" title="">Form wizards</a></li>
                    </ul>
                </li>
                <li class="current"><a href="forms.html" title="">Inputs &amp; elements</a></li>
            </ul>
        </div>

        <div class="breadLinks">
            <ul>
                <li><a href="#" title=""><i class="icos-list"></i><span>Orders</span> <strong>(+58)</strong></a></li>
                <li><a href="#" title=""><i class="icos-check"></i><span>Tasks</span> <strong>(+12)</strong></a></li>
                <li class="has">
                    <a title="">
                        <i class="icos-money3"></i>
                        <span>Invoices</span>
                        <span><img src="{{ asset('public/images/elements/control/hasddArrow.png') }}" alt="" /></span>
                    </a>
                    <ul>
                        <li><a href="#" title=""><span class="icos-add"></span>New invoice</a></li>
                        <li><a href="#" title=""><span class="icos-archive"></span>History</a></li>
                        <li><a href="#" title=""><span class="icos-printer"></span>Print invoices</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <!-- Main content -->
    <div class="wrapper">
        @include('Admin.clients.middle_nav')

        <fieldset>
            <div class="widget fluid">
                <div class="whead"><h6>Input fields</h6></div>
                <div class="formRow">
                    <div class="grid3"><label>Cargo Type</label></div>
                    <div class="grid9"><select name="styled-dropdown" class="styled">
                        <option value="opt1">Cargo type Option 1</option>
                        <option value="opt2">Option 2</option>
                        <option value="opt3">Option 3</option>
                        <option value="opt4">Option 4</option>
                        <option value="opt5">Option 5</option>
                        <option value="opt6">Option 6</option>
                        <option value="opt7">Option 7</option>
                        <option value="opt8">Option 8</option>
                    </select></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Number of package</label></div>
                    <div class="grid9"><input type="text" name="regular" placeholder="Enter Number" /></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Cargo Type</label></div>
                    <div class="grid9"><select name="styled-dropdown" class="styled">
                        <option value="opt1">Cargo type Option 1</option>
                        <option value="opt2">Option 2</option>
                        <option value="opt3">Option 3</option>
                        <option value="opt4">Option 4</option>
                        <option value="opt5">Option 5</option>
                        <option value="opt6">Option 6</option>
                        <option value="opt7">Option 7</option>
                        <option value="opt8">Option 8</option>
                    </select></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Total weight of cargo</label></div>
                    <div class="grid9"><input type="text" name="regular" placeholder="Enter Number" /></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Total volume of cargo</label></div>
                    <div class="grid9"><input type="text" name="regular" placeholder="Enter Number" /></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>IMO Class</label></div>
                    <div class="grid9"><select name="styled-dropdown" class="styled">
                        <option v   alue="opt1">Cargo type Option 1</option>
                        <option value="opt2">Option 2</option>
                        <option value="opt3">Option 3</option>
                        <option value="opt4">Option 4</option>
                        <option value="opt5">Option 5</option>
                        <option value="opt6">Option 6</option>
                        <option value="opt7">Option 7</option>
                        <option value="opt8">Option 8</option>
                    </select></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>UN Number</label></div>
                    <div class="grid9"><input type="text" name="regular" placeholder="Enter Number" /></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Package Group</label></div>
                    <div class="grid9"><input type="text" name="regular" placeholder="Enter Number" /></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Trade</label></div>
                    <div class="grid9"><select name="styled-dropdown" class="styled">
                        <option v   alue="opt1">Cargo type Option 1</option>
                        <option value="opt2">Option 2</option>
                        <option value="opt3">Option 3</option>
                        <option value="opt4">Option 4</option>
                        <option value="opt5">Option 5</option>
                        <option value="opt6">Option 6</option>
                        <option value="opt7">Option 7</option>
                        <option value="opt8">Option 8</option>
                    </select></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Commodity</label></div>
                    <div class="grid9"><input type="text" name="regular" placeholder="Enter cargo details
                    " /></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>HS code</label></div>
                    <div class="grid9"><input type="text" name="regular" placeholder="Enter HS code" /></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Total value cargo</label></div>
                    <div class="grid9"><input type="text" name="regular" placeholder="Enter HS code" /></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Do you need Transport Insurance</label></div>
                    <div class="grid9"><select name="styled-dropdown" class="styled">
                        <option v   alue="opt1">Cargo type Option 1</option>
                        <option value="opt2">Option 2</option>
                        <option value="opt3">Option 3</option>
                        <option value="opt4">Option 4</option>
                        <option value="opt5">Option 5</option>
                        <option value="opt6">Option 6</option>
                        <option value="opt7">Option 7</option>
                        <option value="opt8">Option 8</option>
                    </select></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Do you have regular shipments</label></div>
                    <div class="grid9"><select name="styled-dropdown" class="styled">
                        <option v   alue="opt1">Cargo type Option 1</option>
                        <option value="opt2">Option 2</option>
                        <option value="opt3">Option 3</option>
                        <option value="opt4">Option 4</option>
                        <option value="opt5">Option 5</option>
                        <option value="opt6">Option 6</option>
                        <option value="opt7">Option 7</option>
                        <option value="opt8">Option 8</option>
                    </select></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Shipping details / Information</label></div>
                    <div class="grid9"><textarea rows="8" cols="" name="textarea"></textarea> </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<!-- Content ends -->

@endsection