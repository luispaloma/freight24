@extends('Admin.layout')

@section('title')
    i-Freight 24 CMS Company
@stop


@section('content')

<!-- Content begins -->
<div id="content">
    <div class="contentTop">
        <span class="pageTitle"><span class="icon-chat-2"></span>Messages layout</span>
        <ul class="quickStats">
            <li>
                <a href="" class="blueImg"><img src="{{ asset('public/images/icons/quickstats/plus.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">5489</strong><span>visits</span></div>
            </li>
            <li>
                <a href="" class="redImg"><img src="{{ asset('public/images/icons/quickstats/user.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">4658</strong><span>users</span></div>
            </li>
            <li>
                <a href="" class="greenImg"><img src="{{ asset('public/images/icons/quickstats/money.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">1289</strong><span>orders</span></div>
            </li>
        </ul>
    </div>

    <!-- Breadcrumbs line -->
    <div class="breadLine">
        <div class="bc">
            <ul id="breadcrumbs" class="breadcrumbs">
                <li><a href="index.html">Dashboard</a></li>
                <li class="current"><a href="messages.html" title="">Messages</a></li>
            </ul>
        </div>

        <div class="breadLinks">
            <ul>
                <li><a href="#" title=""><i class="icos-list"></i><span>Orders</span> <strong>(+58)</strong></a></li>
                <li><a href="#" title=""><i class="icos-check"></i><span>Tasks</span> <strong>(+12)</strong></a></li>
                <li class="has">
                    <a title="">
                        <i class="icos-money3"></i>
                        <span>Invoices</span>
                        <span><img src="{{ asset('public/images/elements/control/hasddArrow.png') }}" alt="" /></span>
                    </a>
                    <ul>
                        <li><a href="#" title=""><span class="icos-add"></span>New invoice</a></li>
                        <li><a href="#" title=""><span class="icos-archive"></span>History</a></li>
                        <li><a href="#" title=""><span class="icos-printer"></span>Print invoices</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <!-- Main content -->
    <div class="wrapper">
        <ul class="middleNavR">
            <li><a href="#" title="Add an article" class="tipN"><img src="{{ asset('public/images/icons/middlenav/create.png') }}" alt="" /></a></li>
            <li><a href="#" title="Upload files" class="tipN"><img src="{{ asset('public/images/icons/middlenav/upload.png') }}" alt="" /></a></li>
            <li><a href="#" title="Add something" class="tipN"><img src="{{ asset('public/images/icons/middlenav/add.png') }}" alt="" /></a></li>
            <li><a href="#" title="Messages" class="tipN"><img src="{{ asset('public/images/icons/middlenav/dialogs.png') }}" alt="" /></a><strong>8</strong></li>
            <li><a href="#" title="Check statistics" class="tipN"><img src="{{ asset('public/images/icons/middlenav/stats.png') }}" alt="" /></a></li>
        </ul>

        <!-- Messages #1 -->
        <div class="widget">
            <div class="whead">
                <h6>Messages layout #1</h6>
                <div class="on_off">
                    <span class="icon-reload-CW"></span>
                    <input type="checkbox" id="check1" checked="checked" name="chbox" />
                </div>
            </div>

            <ul class="messagesOne">
                <li class="by_user">
                    <a href="#" title=""><img src="{{ asset('public/images/live/face1.png') }}" alt="" /></a>
                    <div class="messageArea">
                        <span class="aro"></span>
                        <div class="infoRow">
                            <span class="name"><strong>John</strong> says:</span>
                            <span class="time">3 hours ago</span>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel est enim, vel eleifend felis. Ut volutpat, leo eget euismod scelerisque, eros purus lacinia velit, nec rhoncus mi dui eleifend orci.
                        Phasellus ut sem urna, id congue libero. Nulla eget arcu vel massa suscipit ultricies ac id velit
                    </div>
                </li>

                <li class="divider"><span></span></li>

                <li class="by_me">
                    <a href="#" title=""><img src="{{ asset('public/images/live/face2.png') }}" alt="" /></a>
                    <div class="messageArea">
                        <span class="aro"></span>
                        <div class="infoRow">
                            <span class="name"><strong>Eugene</strong> says:</span>
                            <span class="time">3 hours ago</span>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel est enim, vel eleifend felis. Ut volutpat, leo eget euismod scelerisque, eros purus lacinia velit, nec rhoncus mi dui eleifend orci.
                        Phasellus ut sem urna, id congue libero. Nulla eget arcu vel massa suscipit ultricies ac id velit
                    </div>
                </li>

                <li class="by_me">
                    <a href="#" title=""><img src="{{ asset('public/images/live/face2.png') }}" alt="" /></a>
                    <div class="messageArea">
                        <span class="aro"></span>
                        <div class="infoRow">
                            <span class="name"><strong>Eugene</strong> says:</span>
                            <span class="time">3 hours ago</span>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel est enim, vel eleifend felis. Ut volutpat, leo eget euismod scelerisque, eros purus lacinia velit, nec rhoncus mi dui eleifend orci.
                        Phasellus ut sem urna, id congue libero. Nulla eget arcu vel massa suscipit ultricies ac id velit
                    </div>
                </li>

                <li class="divider"><span></span></li>

                <li class="by_user">
                    <a href="#" title=""><img src="{{ asset('public/images/live/face1.png') }}" alt="" /></a>
                    <div class="messageArea">
                        <span class="aro"></span>
                        <div class="infoRow">
                            <span class="name"><strong>John</strong> says:</span>
                            <span class="time">3 hours ago</span>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel est enim, vel eleifend felis. Ut volutpat, leo eget euismod scelerisque, eros purus lacinia velit, nec rhoncus mi dui eleifend orci.
                        Phasellus ut sem urna, id congue libero. Nulla eget arcu vel massa suscipit ultricies ac id velit
                    </div>
                </li>

                <li class="divider"><span></span></li>

                <li class="by_me">
                    <a href="#" title=""><img src="{{ asset('public/images/live/face2.png') }}" alt="" /></a>
                    <div class="messageArea">
                        <span class="aro"></span>
                        <div class="infoRow">
                            <span class="name"><strong>Eugene</strong> says:</span>
                            <span class="time">3 hours ago</span>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel est enim, vel eleifend felis. Ut volutpat, leo eget euismod scelerisque, eros purus lacinia velit, nec rhoncus mi dui eleifend orci.
                        Phasellus ut sem urna, id congue libero. Nulla eget arcu vel massa suscipit ultricies ac id velit
                    </div>
                </li>
            </ul>
        </div>

        <!-- Enter message field -->
        <div class="enterMessage">
            <input type="text" name="enterMessage" placeholder="Enter your message..." />
            <div class="sendBtn">
                <a href="#" title="" class="attachPhoto"></a>
                <a href="#" title="" class="attachLink"></a>
                <input type="submit" name="sendMessage" class="buttonS bLightBlue" value="Send" />
            </div>
        </div>

        <div class="divider"><span></span></div>


        <!-- Messages #2 -->
        <div class="widget">
            <div class="whead">
                <h6>Messages layout #2</h6>
                <div class="on_off">
                    <span class="icon-reload-CW"></span>
                    <input type="checkbox" name="chbox" />
                </div>
            </div>

            <ul class="messagesTwo">
                <li class="by_user">
                    <a href="#" title=""><img src="{{ asset('public/images/live/face1.png') }}" alt="" /></a>
                    <div class="messageArea">
                        <div class="infoRow">
                            <span class="name"><strong>Jonathan</strong> says:</span>
                            <span class="time">3 hours ago</span>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel est enim, vel eleifend felis. Ut volutpat, leo eget euismod scelerisque, eros purus lacinia velit, nec rhoncus mi dui eleifend orci.
                        Phasellus ut sem urna, id congue libero. Nulla eget arcu vel massa suscipit ultricies ac id velit
                    </div>
                </li>

                <li class="by_me">
                    <a href="#" title=""><img src="{{ asset('public/images/live/face2.png') }}" alt="" /></a>
                    <div class="messageArea">
                        <div class="infoRow">
                            <span class="name"><strong>Eugene</strong> says:</span>
                            <span class="time">3 hours ago</span>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel est enim, vel eleifend felis. Ut volutpat, leo eget euismod scelerisque, eros purus lacinia velit, nec rhoncus mi dui eleifend orci.
                        Phasellus ut sem urna, id congue libero. Nulla eget arcu vel massa suscipit ultricies ac id velit
                    </div>
                </li>

                <li class="by_me">
                    <a href="#" title=""><img src="{{ asset('public/images/live/face2.png') }}" alt="" /></a>
                    <div class="messageArea">
                        <div class="infoRow">
                            <span class="name"><strong>Eugene</strong> says:</span>
                            <span class="time">3 hours ago</span>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel est enim, vel eleifend felis. Ut volutpat, leo eget euismod scelerisque, eros purus lacinia velit, nec rhoncus mi dui eleifend orci.
                        Phasellus ut sem urna, id congue libero. Nulla eget arcu vel massa suscipit ultricies ac id velit
                    </div>
                </li>

                <li class="by_user">
                    <a href="#" title=""><img src="{{ asset('public/images/live/face1.png') }}" alt="" /></a>
                    <div class="messageArea">
                        <div class="infoRow">
                            <span class="name"><strong>Jonathan</strong> says:</span>
                            <span class="time">3 hours ago</span>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel est enim, vel eleifend felis. Ut volutpat, leo eget euismod scelerisque, eros purus lacinia velit, nec rhoncus mi dui eleifend orci.
                        Phasellus ut sem urna, id congue libero. Nulla eget arcu vel massa suscipit ultricies ac id velit
                    </div>
                </li>

                <li class="by_me">
                    <a href="#" title=""><img src="{{ asset('public/images/live/face2.png') }}" alt="" /></a>
                    <div class="messageArea">
                        <div class="infoRow">
                            <span class="name"><strong>Eugene</strong> says:</span>
                            <span class="time">3 hours ago</span>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel est enim, vel eleifend felis. Ut volutpat, leo eget euismod scelerisque, eros purus lacinia velit, nec rhoncus mi dui eleifend orci.
                        Phasellus ut sem urna, id congue libero. Nulla eget arcu vel massa suscipit ultricies ac id velit
                    </div>
                </li>
            </ul>
        </div>

        <!-- Enter messages field -->
        <div class="enterMessage">
            <input type="text" name="enterMessage" placeholder="Enter your message..." />
            <div class="sendBtn">
                <a href="#" title="" class="attachPhoto"></a>
                <a href="#" title="" class="attachLink"></a>
                <input type="submit" name="sendMessage" class="buttonS bLightBlue" value="Send" />
            </div>
        </div>
    </div>
    <!-- Main content ends -->

</div>
<!-- Content ends -->

@endsection