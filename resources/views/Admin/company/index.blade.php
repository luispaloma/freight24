@extends('Admin.layout')

@section('title')
    i-Freight 24 CMS Company
@stop


@section('content')

<!-- Content begins -->
<div id="content">
    <div class="contentTop">
        <span class="pageTitle">
        	<span class="icon-user-2"></span>User interface elements
    	</span>
        <ul class="quickStats">
            <li>
                <a href="" class="blueImg">
                	<img src="{{ asset('public/images/icons/quickstats/plus.png') }}" alt="" />
            	</a>
                <div class="floatR"><strong class="blue">5489</strong><span>visits</span></div>
            </li>
            <li>
                <a href="" class="redImg"><img src="{{ asset('public/images/icons/quickstats/user.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">4658</strong><span>users</span></div>
            </li>
            <li>
                <a href="" class="greenImg"><img src="{{ asset('public/images/icons/quickstats/money.png') }}" alt="" /></a>
                <div class="floatR"><strong class="blue">1289</strong><span>orders</span></div>
            </li>
        </ul>
    </div>

    <!-- Breadcrumbs line -->
    <div class="breadLine">
        <div class="bc">
            <ul id="breadcrumbs" class="breadcrumbs">
                <li>
                    <a href="#">Dashboard</a>
                </li>
                <li>
                    <a href="#">UI elements</a>
                    <ul>
                        <li><a href="ui_icons.html" title="">Icons</a></li>
                        <li><a href="ui_buttons.html" title="">Button sets</a></li>
                        <li><a href="ui_grid.html" title="">Grid</a></li>
                        <li><a href="ui_custom.html" title="">Custom elements</a></li>
                    </ul>
                </li>
                <li class="current"><a href="company.html" title="">General elements</a></li>
            </ul>
        </div>

        <div class="breadLinks">
            <ul>
                <li><a href="#" title=""><i class="icos-list"></i><span>Orders</span> <strong>(+58)</strong></a></li>
                <li><a href="#" title=""><i class="icos-check"></i><span>Tasks</span> <strong>(+12)</strong></a></li>
                <li class="has">
                    <a title="">
                        <i class="icos-money3"></i>
                        <span>Invoices</span>
                        <span><img src="images/elements/control/hasddArrow.png" alt="" /></span>
                    </a>
                    <ul>
                        <li><a href="#" title=""><span class="icos-add"></span>New invoice</a></li>
                        <li><a href="#" title=""><span class="icos-archive"></span>History</a></li>
                        <li><a href="#" title=""><span class="icos-printer"></span>Print invoices</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <!-- Main content -->
    <div class="wrapper">

        <!-- Rounded buttons -->


        <!-- Square buttons -->
        <div class="divider"><span></span></div>

        <!-- Alternative buttons -->
        <ul class="middleNavA">
            <li><a href="#" title="Add an article"><img src="{{ asset('public/images/icons/color/order-149.png') }}" alt="" /><span>Freight type</span></a></li>
            <li><a href="#" title="Upload files"><img src="{{ asset('public/images/icons/color/issue.png') }}" alt="" /><span>My invoices</span></a></li>
            <li><a href="#" title="Add something"><img src="{{ asset('public/images/icons/color/hire-me.png') }}" alt="" /><span>Add users</span></a><strong>8</strong></li>
            <li><a href="#" title="Messages"><img src="{{ asset('public/images/icons/color/donate.png') }}" alt="" /><span>Add Files</span></a></li>
            <li><a href="#" title="Check statistics"><img src="{{ asset('public/images/icons/color/config.png') }}" alt="" /><span>Adjustments</span></a></li>
        </ul>

        <div class="divider"><span></span></div>
        <form action="{{ route('admin.company.save') }}" method="POST">
            {{ csrf_field() }}
            <fieldset>
                <div class="widget fluid">
                    <div class="whead"><h6>Company form</h6></div>
                    <div class="formRow">
                        <div class="grid3"><label>Company name</label></div>
                        <div class="grid9"><input type="text" name="regular" /></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Company Number ID</label></div>
                        <div class="grid9"><input type="text" name="regular" /></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Cargo Type</label></div>
                        <div class="grid9">
                            <select name="styled-dropdown" class="styled">
                                <option value="opt1">Cargo type Option 1</option>
                                <option value="opt2">Option 2</option>
                                <option value="opt3">Option 3</option>
                                <option value="opt4">Option 4</option>
                                <option value="opt5">Option 5</option>
                                <option value="opt6">Option 6</option>
                                <option value="opt7">Option 7</option>
                                <option value="opt8">Option 8</option>
                            </select>
                        </div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Number of package</label></div>
                        <div class="grid9"><input type="text" name="regular" /></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Cargo Type 2</label></div>
                        <div class="grid9">
                            <select name="styled-dropdown" class="styled">
                                <option value="opt1">Cargo type Option 1</option>
                                <option value="opt2">Option 2</option>
                                <option value="opt3">Option 3</option>
                                <option value="opt4">Option 4</option>
                                <option value="opt5">Option 5</option>
                                <option value="opt6">Option 6</option>
                                <option value="opt7">Option 7</option>
                                <option value="opt8">Option 8</option>
                            </select>
                        </div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Total weight of cargo available</label></div>
                        <div class="grid9"><input type="text" name="regular" /></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Total volume of cargo available</label></div>
                        <div class="grid9"><input type="text" name="regular" /></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>IMO Class<label></div>
                        <div class="grid9">
                            <select name="styled-dropdown" class="styled">
                                <option value="opt1">Cargo type Option 1</option>
                                <option value="opt2">Option 2</option>
                                <option value="opt3">Option 3</option>
                                <option value="opt4">Option 4</option>
                                <option value="opt5">Option 5</option>
                                <option value="opt6">Option 6</option>
                                <option value="opt7">Option 7</option>
                                <option value="opt8">Option 8</option>
                            </select>
                        </div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>UN Number<span class="note">The same stuff here</span></label></div>
                        <div class="grid9"><input type="text" name="regular" /></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Trade</label></div>
                        <div class="grid9">
                            <select name="styled-dropdown" class="styled">
                                <option value="opt1">Cargo type Option 1</option>
                                <option value="opt2">Option 2</option>
                                <option value="opt3">Option 3</option>
                                <option value="opt4">Option 4</option>
                                <option value="opt5">Option 5</option>
                                <option value="opt6">Option 6</option>
                                <option value="opt7">Option 7</option>
                                <option value="opt8">Option 8</option>
                            </select>
                        </div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Commodity</label></div>
                        <div class="grid9"><input type="text" name="regular" /></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>HS Code</label></div>
                        <div class="grid9"><input type="text" name="regular" /></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Shipping details / Information</label></div>
                        <div class="grid9"><textarea rows="8" cols="" name="textarea"></textarea> </div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"></div>
                        <div class="grid9">
                            <input type="submit" class="buttonS bGreen" value="Submit">
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
        <div class="divider"><span></span></div>
        <div class="widget grid6">
            <div class="whead"><h6>Information</h6></div>
            <div class="tabs rightTabs">
                <ul>
                    <li><a href="#tabs-10">Nunc tincidunt</a></li>
                    <li><a href="#tabs-11">Proin dolor</a></li>
                    <li><a href="#tabs-12">Aenean lacinia</a></li>
                </ul>
                <div id="tabs-10">
                    Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.
                </div>
                <div id="tabs-11">
                    Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.
                </div>
                <div id="tabs-12">
                    Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.
                    <p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection