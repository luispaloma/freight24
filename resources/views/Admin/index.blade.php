@extends('Admin.layout')

@section('title')
	i-Freight 24 CMS
@stop

@section('content')
	<!-- Login wrapper begins -->
	<div class="loginWrapper">

		<!-- Current user form -->
		<form action="{{ route('admin.login') }}" id="login" method="POST" class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
			{{ csrf_field() }}
			<div class="loginPic">
				<a href="#" title="">
					<img src="{{ asset('public/images/userLogin2.png') }}" alt="" />
				</a>
				<span>Admin User</span>
				<div class="loginActions">
					<div>
						<a href="#" title="Change user" class="logleft flip"></a>
					</div>
					<div>
						<a href="#" title="Forgot password?" class="logright"></a>
					</div>
				</div>
			</div>

			<input type="text" name="email" placeholder="Confirm your email" class="loginEmail" value="{{ old('email') }}" />
			@if ($errors->has('email'))
			    <span class="help-block">
			        <strong>{{ $errors->first('email') }}</strong>
			    </span>
			@endif
			<input type="password" name="password" placeholder="Password" class="loginPassword" />
			@if ($errors->has('password'))
			    <span class="help-block">
			        <strong>{{ $errors->first('password') }}</strong>
			    </span>
			@endif

			<div class="logControl">
				<div class="memory">
					<input type="checkbox" class="check" id="remember1" />
					<label for="remember1">Remember me</label>
				</div>
				<input type="submit" name="submit" value="Login" class="buttonM bBlue" />
			</div>
		</form>

	</div>
	<!-- Login wrapper ends -->
@stop

