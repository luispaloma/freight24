@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'Philosophy'])

<div class="container-fluid inner-offset">
	<div class="hgroup text-center wow zoomIn" data-wow-delay="0.3s">
		<h2>Philosophy</h2>
		<h1>Philosophy</h1>
	</div>


	<div class="tab-content inner-offset wow fadeIn" data-wow-delay="0.3s">
		<div class="tab-pane active" id="tab1">
			<div class="row">

				<div class="col-sm-9 col-md-offset-2">
					<h3>What is i-Freight24</h3>
					<p>In General: We want to bring shippers/consignees and forwarders/carriers together</p>
					<p>i-Freight24 for shippers provides you a simple online platform where you can easily place your freight request and you get various offers from different forwarders. This allows you to smoothly compare the rates and choose the best bid for your business.</p>
					<p>i-Freight24 for forwarders and carriers will offer you a huge range of freight requests for all transport modes. Look for the most interesting request for your company and place your offer. Very soon you will get new orders and potentially new customers.</p>
					<h3>i-Freight24</h3>
					<p>In General: Extend your network and raise your facts and figures</p>
					<p>-Freight24 for shippers: Our platform will work for you. Don’t ask several forwarders anymore by mail to get a quote for your request. Place your request on iFreight24 and you will get the best market bid from specialized forwarding companies within your given timeframe.</p>
					<p>i-Freight24 for forwarders and carriers: looking for additional orders and new customers? Visit i-Freight24 and look up for transport requests which match your business opportunities. You will have the chance to use your capacity and raise your profit.</p>
					<h3>i-Freight24 Client base</h3>
					<p>Our client base ranges from one time shipper/consignees, to daily
						shipper/consignees. On forwarder/carrier side we operate with global players, local
					and regional specialists and dedicated business partners.</p>
				</div>
			</div>
		</div>
	</div>
</div>



@endsection