<div class="container-fluid block-content percent-blocks" data-waypoint-scroll="true">
	<div class="row stats">
		<div class="col-sm-6 col-md-3 col-lg-3">
			<div class="chart" data-percent="230">
				<span><i class="fa fa-folder-open"></i></span> <span class="percent"></span>Projects Done
			</div>
		</div>
		<div class="col-sm-6 col-md-3 col-lg-3">
			<div class="chart" data-percent="68">
				<span><i class="fa fa-users"></i></span> <span class="percent"></span>Clients Worldwide
			</div>
		</div>
		<div class="col-sm-6 col-md-3 col-lg-3">
			<div class="chart" data-percent="147">
				<span><i class="fa fa-truck"></i></span> <span class="percent"></span>Official Branches
			</div>
		</div>
		<div class="col-sm-6 col-md-3 col-lg-3">
			<div class="chart" data-percent="105">
				<span><i class="fa fa-male"></i></span> <span class="percent"></span>Satisfied Clients
			</div>
		</div>
	</div>
</div>