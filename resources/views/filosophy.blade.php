@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'Philosophy'])

<div class="container-fluid inner-offset">
	<div class="hgroup text-center wow zoomIn" data-wow-delay="0.3s">

		<h1>Philosophy</h1>
	</div>

	<div class="tab-content inner-offset wow fadeIn" data-wow-delay="0.3s">
		<div class="tab-pane active" id="tab1">
			<div class="row">

				<div class="col-sm-9 col-md-offset-2">
					<h3>Our Team</h3>
					<p>You are in our team and help us and other customers with real feedbacks to get the best carriers with the best prices. Over 1000 people right across the world are dedicated to serving i-Freight24.com customers and cargo agents. </p>
					<h3>Our Mission</h3>
					<p>To help customers, whatever their budgets, easily send their goods around the world, book, and track their loads.</p>
					<h3>Our Vision</h3>
					<p>i-Freight24.com is an informative, user-friendly website that guarantees the best available prices. Our goal is to provide business with the most accessible and cost-effective way of searching and booking of containers for customers who send their goods to every corner of the world.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="hgroup text-center wow zoomIn" data-wow-delay="0.3s">
		<h2></h2>
		<h2>BENEFITS FOR OUR CUSTOMERS</h2>
	</div>

	<div class="tab-content inner-offset wow fadeIn" data-wow-delay="0.3s">
		<div class="tab-pane active" id="tab1">
			<div class="row">

				<div class="col-sm-9 col-md-offset-2">
					<h3>Lowest rates</h3>
					<p>Whether you’re sending your goods, i-Freight24 guarantees you the best available rates on delivery.</p>
					<h3>NO BOOKING COST</h3>
					<p>i-Freight24.com booking service is for free. We do not charge any booking fees or add any adminmistration fees on your quotes.</p>
					<h3>BENEFITS FOR OUR PARTNER / CARGO AGENTS</h3>
					<p>i-Freight24 provides a cost-effective platform for your requests. We helo cargo agents optimize their revenue and their capacities. </p>
					<p>For further information please feel free to <a href="#">Contact us.</a></p>
				</div>
			</div>
		</div>
	</div>

</div>

@endsection