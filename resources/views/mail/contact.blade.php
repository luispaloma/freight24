<p>
	<b>First Name: </b>{{ $contact['user_name'] }}
</p>
<p>
	<b>Last Name: </b>{{ $contact['user_lastname'] }}
</p>
<p>
	<b>Email: </b>{{ $contact['user_email'] }}
</p>
<p>
	<b>Phone: </b>{{ $contact['user_phone'] }}
</p>
<p>
	<b>Subject: </b>{{ $contact['user_subject'] }}
</p>
<p>
	<b>Message: </b>{{ $contact['user_message'] }}
</p>