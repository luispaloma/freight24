@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'Register'])

	<div class="row login_form">
		<h1 style="text-align:center">NEW USER REGISTRATION</h1>
		<div class="">
			<section class="container">
				<div class="login">
					<form id="register_form" method="post" action="{{ route('register') }}">
						{{ csrf_field() }}
						@if (count($errors) > 0)
						<div class="row">
							<div class="col-sm-12">
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							</div>
						</div>
						@endif

						<div class="col-xs-6">
							<p>Enter your details</p>
							<p>
								<input class="login-form-input" type="text" name="email" placeholder=" Email" required="required">
							</p>
							<p>
								<input class="login-form-input" type="text" name="first_name" value="" placeholder=" First Name" required="required">
							</p>
							<p>
								<input class="login-form-input" type="text" name="last_name" value="" placeholder=" Last Name" required="required">
							</p>
							<p>
								<input class="login-form-input" type="text" name="company" value="" placeholder=" Company Name" required="required">
							</p>
							<p>
								<input class="login-form-input" type="text" name="address" value="" placeholder=" Address" required="required">
							</p>
						</div>
						<div class="col-xs-6">
							<p>
								<input class="login-form-input" type="text" name="city" value="" placeholder=" City" required="required">
							</p>
							<p>
								<input class="login-form-input" type="text" name="plz" value="" placeholder=" PLZ" required="required">
							</p>
							<p>
								<input class="login-form-input" type="text" name="country" value="" placeholder=" Country" required="required">
							</p>
							<p>
								<input class="login-form-input" type="text" name="phone_number" value="" placeholder=" Phone Number" required="required">
							</p>
							<p>
								<input class="login-form-input" type="password" name="password" value="" placeholder=" Password" required="required">
							</p>
							<p>
								<input class="login-form-input" type="password" name="password_confirmation" value="" placeholder=" Repeat Password" required="required">
							</p>
							<p class="remember_me">
								<label>
									<input type="checkbox" name="remember_me" id="remember_me" required="required">
									I am agree with <a href="{{ route('index.terms') }}" target="_blank">terms and conditions</a>
								</label>
							</p>
							<p class="submit">
								<input class="btn btn-success" type="submit" name="commit" value="Register">
							</p>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>

@endsection

@section('custom_scripts')
    <script type="text/javascript">
		$("#register_form").validate();
    </script>
@endsection