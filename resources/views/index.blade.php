@extends('layouts.web_layout')
@section('content')
<div id="owl-main-slider" class="owl-carousel enable-owl-carousel" data-single-item="true" data-pagination="false" data-auto-play="true" data-main-slider="true" data-stop-on-hover="true">
    <div class="item">
        <img src="{{ asset('public/css/front/media/main-slider/1.jpg') }}" alt="Img">
        <div class="container-fluid">
            <div class="slider-content">
                <div>
                    <div>
                        <h1>i-<span>FREIGHT</span> 24 </h1>
                        <h6>24 HOUR SERVICE</h6>
                        <h3>FACILITATE THE SHIPPING OF GREAT ITEMS</h3>
                    </div>
                </div>
                <p>I-Freight24 is the leader in providing the best prices for transports worldwide<br><a class="btn btn-success" href="{{ route('register') }}">SEND ME A QUOTE</a></p>
                <div style="display:table-cell; width:100px; vertical-align:top;">
                    <a class="prev"><i class="fa fa-angle-left"></i></a>
                    <a class="next"><i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
@include('carousel')
<div class="container-fluid block-content">
    <div class="hgroup text-center wow zoomInUp" data-wow-delay="0.3s">
        <h1>OUR CLIENT'S FEEDBACK</h1>
        <h2>Read the latest feedbacks from our clients.</h2>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12 wow fadeInLeft" data-wow-delay="0.3s">
            <div id="testimonials" class="owl-carousel enable-owl-carousel" data-single-item="true" data-pagination="false" data-navigation="true" data-auto-play="true">
                <div>
                    <div class="testimonial-content">
                        <span><i class="fa fa-quote-left"></i></span>
                        <p>We love the approachable format, and the fact that they chose to feature customers that users can really relate to. When you click into any story, you can read the whole case study in a Q&A format.</p>
                    </div>
                    <div class="text-right testimonial-author">
                        <h4>JOHN DEO</h4>
                        <small>Managing Director</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid block-content">
    <div class="hgroup text-center wow zoomInUp" data-wow-delay="0.3s">
        <h1>OUR SERVICES</h1>
        <h2>We have a wide network of global and local customers and logistics provider around the world.<br/> This will help you to find the right partner.</h2>
    </div>
    <div class="row our-services styled">
        <div class="col-sm-6 wow zoomInLeft" data-wow-delay="0.3s">
            <a href="08_services-details.html">
                <span><i class="glyph-icon flaticon-boats4"></i></span>
                <h4>SEA FREIGHT</h4>
                <p>Request your freight quotes for LCL and FCL shipments.</p>
            </a>
        </div>
        <div class="col-sm-6 wow zoomInRight" data-wow-delay="0.3s">
            <a href="08_services-details.html">
                <span><i class="glyph-icon flaticon-flying"></i></span>
                <h4>AIR FREIGHT</h4>
                <p>Request your freight quotes for shipments by air.</p>
            </a>
        </div>
        <div class="col-sm-6 wow zoomInRight" data-wow-delay="0.3s">
            <a href="08_services-details.html">
                <span><i class="glyph-icon flaticon-traffic-signal"></i></span>
                <h4>ROAD FREIGHT</h4>
                <p>Request your for inland and international transport by LTL or FTL.</p>
            </a>
        </div>
        <div class="col-sm-6 wow zoomInRight" data-wow-delay="0.3s">
            <a href="08_services-details.html">
                <span><i class="glyph-icon flaticon-package7"></i></span>
                <h4>BREAKBULK</h4>
                <p>By Breakbulk you can get your quote for oversized or overweight cargo which is too big or too heavy for a Container.</p>
            </a>
        </div>
        <div class="col-sm-6 wow zoomInRight" data-wow-delay="0.3s">
            <a href="08_services-details.html">
                <span><i class="glyph-icon flaticon-railway1"></i></span>
                <h4>RAIL FREIGHT</h4>
                <p>Request a freight quote for railway.</p>
            </a>
        </div>
        <div class="col-sm-6 wow zoomInRight" data-wow-delay="0.3s">
            <a href="08_services-details.html">
                <span><i class="glyph-icon flaticon-railway1"></i></span>
                <h4>WAREHOUSE</h4>
                <p>Request your storage quote for your cargo. </p>
            </a>
        </div>
    </div>
</div>
<div class="container-fluid block-content">
    <div class="hgroup text-center wow zoomInUp" data-wow-delay="0.3s">
        <h1>LOG IN CHARGES</h1>
        <h2>LOWEST REGISTRATION RATE – HIGHEST PROFIT</h2>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-6 wow fadeInLeft" data-wow-delay="0.3s">
            <img style="width: 500px" src="{{ asset('public/css/front/img/chargesimg.jpg') }}" alt="">
        </div>
        <div class="col-md-6 col-lg-6 wow fadeInRight" data-wow-delay="0.3s">
            <div class="hgroup">
                <h1>FIRST 5 QUOTES FREE</h1>
            </div>
            <ul class="why-us">
                <li>Light <strong>1 PRODUCT</strong>
                    <p>1 MONTH    45.00 CHF / USD / EUR <br/> <button>buy</button></p>
                    <p>3 MONTH  135.00 CHF / USD / EUR <br/> <button>buy</button></p>
                    <p>6 MONTH  250.00 CHF / USD / EUR <br/> <button>buy</button></p>
                    <p>12 MONTH 450.00 CHF / USD / EUR <br/><button>buy</button></p>
                    <span>+</span>
                </li>
                <li>BUSINESS <strong>2 PRODUCTS</strong>
                    <p>1 MONTH    55.00 CHF / USD / EUR  <br/> <button>buy</button></p>
                    <p>3 MONTH  165.00 CHF / USD / EUR <br/><button>buy</button></p>
                    <p>6 MONTH  305.00 CHF / USD / EUR <br/> <button>buy</button></p>
                    <p>12 MONTH 550.00 CHF / USD / EUR <br/><button>buy</button></p>
                    <span>+</span>
                </li>
                <li>BUSINESS <strong>6 PRODUCTS</strong>
                    <p>1 MONTH    65.00 CHF / USD / EUR  <br/> <button>buy</button></p>
                    <p>3 MONTH  195.00 CHF / USD / EUR <br/><button>buy</button></p>
                    <p>6 MONTH  360.00 CHF / USD / EUR <br/> <button>buy</button></p>
                    <p>12 MONTH 650.00 CHF / USD / EUR <br/><button>buy</button></p>
                    <span>+</span>
                </li>
            </ul>
        </div>
    </div>
</div>
<hr>
<div class="container-fluid inner-offset">

    <div class="container filter_controls">
        <div class="row shipping-box">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <img src="{{ asset('public/css/front/img/less.png') }}">
                        <h3 class="mt10">Less container load</h3>
                    </div>
                    <div class="panel-footer br-n no-padding">
                        <div class="block__roll">
                            <ul class="leadsCycleContainer_exchange">
                                <li style="height: 33px; opacity: 1; list-style: none;">
                                    <a class="msidebar" target="_blank" href="#">
                                        <div class="media">
                                            <div class="media-left media-middle">
                                                <span class="date-it">
                                                    <span class="last1">25 min</span>
                                                </span>
                                            </div>
                                            <div class="media-body">
                                                <i class="flag-icon flag-icon-us"></i>
                                                <span class="direction-text">83-38 Woodhaven Blvd</span>
                                                <i class="flag-icon flag-icon-co"></i>
                                                <span class="direction-text">Cartagena</span></div>
                                            </div>
                                        </a>
                                    </li>
                                    <li style="height: 33px; opacity: 1; list-style: none;">
                                        <a class="msidebar" target="_blank" href="/shipping/list/circuito_de_las_universidad_60_centro_60950_lzaro_crdenas_mich_mexico_mx/caldera_provincia_de_puntarenas_espritu_santo_costa_rica_cr_1">
                                            <div class="media"><div class="media-left media-middle">
                                                <span class="date-it">
                                                    <span class="last1">1 hour</span>
                                                </span>
                                            </div>
                                            <div class="media-body">
                                                <i class="flag-icon flag-icon-mx"></i>
                                                <span class="direction-text">Circuito De Las Universidad 60</span>
                                                <i class="flag-icon flag-icon-cr"></i>
                                                <span class="direction-text">Caldera</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/620_8th_ave__1_new_york_ny_10018_usa_us/cairo_cairo_governorate_egypt_eg_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">1 hour</span></span></div><div class="media-body"><i class="flag-icon flag-icon-us"></i> <span class="direction-text">620 8th Ave # 1</span><i class="flag-icon flag-icon-eg"></i> <span class="direction-text">Cairo</span></div></div></a></li>
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/agadir_ma/sausalito_us_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">2 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-ma"></i> <span class="direction-text">Agadir</span><i class="flag-icon flag-icon-us"></i> <span class="direction-text">Sausalito</span></div></div></a></li>
                                <li style="height: 33px; opacity: 1;list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/santo_domingo_do/santo_domingo_do_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">5 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-do"></i> <span class="direction-text">Santo Domingo</span><i class="flag-icon flag-icon-do"></i> <span class="direction-text">Santo Domingo</span></div></div></a></li>
                            </ul>                    </div>
                        </div>
                    </div>
                    <!--div class="text-center">
                    <a href="/shipping/list/">All Shipping Leads</a>
                </div-->
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <img src="{{ asset('public/css/front/img/fullContainer.png') }}">
                        <h3 class="mt10">Full container load</h3>

                    </div>
                    <div class="panel-footer br-n no-padding">
                        <div class="block__roll">
                            <ul class="leadsCycleContainer_exchange">
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/bandar_abbas_port_ir/ho_chi_minh_city_port_vn_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">59 min</span></span></div><div class="media-body"><i class="flag-icon flag-icon-ir"></i> <span class="direction-text">Bandar Abbas</span><i class="flag-icon flag-icon-vn"></i> <span class="direction-text">Ho Chi Minh City</span></div></div></a></li>
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/altamira_port_mx/hamburg_port_de_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">1 hour</span></span></div><div class="media-body"><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Altamira</span><i class="flag-icon flag-icon-de"></i> <span class="direction-text">Hamburg</span></div></div></a></li>
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/houston_port_us/zeebgrugge_port_be_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">1 hour</span></span></div><div class="media-body"><i class="flag-icon flag-icon-us"></i> <span class="direction-text">Houston</span><i class="flag-icon flag-icon-be"></i> <span class="direction-text">Zeebgrugge</span></div></div></a></li>
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/altamira_port_mx/hamburg_port_de_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">2 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Altamira</span><i class="flag-icon flag-icon-de"></i> <span class="direction-text">Hamburg</span></div></div></a></li>
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/prince_rupert_port_ca/mumbai_port_in_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">2 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-ca"></i> <span class="direction-text">Prince Rupert</span><i class="flag-icon flag-icon-in"></i> <span class="direction-text">Mumbai</span></div></div></a></li>
                            </ul>                    </div>
                        </div>
                    </div>
                    <!--div class="text-center">
                    <a href="/shipping/list/">All Shipping Leads</a>
                </div-->
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <img src="{{ asset('public/css/front/img/in-Bulk.png') }}" alt="">
                        <h3 class="mt10">In Bulk</h3>

                    </div>
                    <div class="panel-footer br-n no-padding">
                        <div class="block__roll">
                            <ul class="leadsCycleContainer_exchange">
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="#"><div class="media"><div class="media-left media-middle"><span class="last1">3 days</span></div><div class="media-body"><i class="flag-icon flag-icon-ua"></i> <span class="direction-text">Yuzhnyy</span><i class="flag-icon flag-icon-my"></i> <span class="direction-text">Port Kelang</span></div></div></a></li>
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/novorossiysk_port_ru/kaohsiung_port_tw_1"><div class="media"><div class="media-left media-middle"><span class="last1">3 days</span></div><div class="media-body"><i class="flag-icon flag-icon-ru"></i> <span class="direction-text">Novorossiysk</span><i class="flag-icon flag-icon-tw"></i> <span class="direction-text">Kaohsiung</span></div></div></a></li>
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/altamira_port_mx/altamira_port_mx_1"><div class="media"><div class="media-left media-middle"><span class="last1"><span class="last1">23 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Altamira</span><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Altamira</span></div></div></a></li>
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/manzanillo_port_mx/yokohama_port_jp_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">23 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Manzanillo</span><i class="flag-icon flag-icon-jp"></i> <span class="direction-text">Yokohama</span></div></div></a></li>
                                <li style="height: 33px; opacity: 1; list-style: none;"><a class="msidebar" target="_blank" href="/shipping/list/manzanillo_port_mx/yokohama_port_jp_1"><div class="media"><div class="media-left media-middle"><span class="date-it"><span class="last1">23 hours</span></span></div><div class="media-body"><i class="flag-icon flag-icon-mx"></i> <span class="direction-text">Manzanillo</span><i class="flag-icon flag-icon-jp"></i> <span class="direction-text">Yokohama</span></div></div></a></li>
                            </ul>                    </div>
                        </div>
                    </div>
                    <!--div class="text-center">
                    <a href="/shipping/list/">All Shipping Leads</a>
                </div-->
            </div>
        </div>
    </div>
</div>
@endsection