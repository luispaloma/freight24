@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'Sea Freight'])
<style type="text/css">
	.formText {
			height: 56px;
		padding-top: 11px;
		margin-bottom: 0!important;
}
button#dropdownMenu1 {
		margin-bottom: 10px;
}
</style>
<img src="{{ asset('public/css/front/img/sea.png') }}" style="width: 100%" />
<div class="container-fluid block-content">
	<div class="row main-grid">
		@include('layouts.form_sidebar_menu')
		<!--Formulario inicial-->
		<div class="col-sm-9" style="margin-bottom: 50px;">
			<form novalidate id="contactForm" class="reply-form">
				<div class="row">
					<div class="col-xs-3 col-sm-3">
						<h4>Place of loading</h4>
						<input type="text" class="form-control" placeholder="Enter city" >
						<input type="text" class="form-control" placeholder="Zip Code" >
						<h5>location type</h5>
						<select class="form-control">
							@foreach ($countries as $key => $country)
							<option value="{{ $key }}">{{ $country }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-3 col-sm-3">
						<h4>Place of loading</h4>
						<h6>Select Country</h6>
						<select class="form-control">
							@foreach ($countries as $key => $country)
							<option value="{{ $key }}">{{ $country }}</option>
							@endforeach
						</select>
						<h6>Select Port</h6>
						<select class="form-control">
							@foreach ($countries as $key => $country)
							<option value="{{ $key }}">{{ $country }}</option>
							@endforeach
						</select>
						<div class="checkbox">
							<label>
								<input type="checkbox"> On board of the ship
							</label>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3">
						<h4>Place of discharge</h4>
						<h6>Select Country</h6>
						<select class="form-control">
							@foreach ($countries as $key => $country)
							<option value="{{ $key }}">{{ $country }}</option>
							@endforeach
						</select>
						<h6>Select Port</h6>
						<select class="form-control">
							@foreach ($countries as $key => $country)
							<option value="{{ $key }}">{{ $country }}</option>
							@endforeach
						</select>
						<div class="checkbox">
							<label>
								<input type="checkbox"> On board of the ship
							</label>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3"><h4>Place of Discharge</h4>
						<input type="text" class="form-control" placeholder="Enter city" >
						<input type="text" class="form-control" placeholder="Zip Code" >
						<h5>location type</h5>
						<select class="form-control">
							@foreach ($countries as $key => $country)
							<option value="{{ $key }}">{{ $country }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</form>
		</div>
		<!--Formulario secundario-->
		<div class="col-sm-3"></div>
		<div class="col-sm-9">
			<div class="row">
				<div id="success"></div>
				<form novalidate id="contactForm" class="reply-form">
					<div class="col-xs-6">
						<div class="form-group">
							<label class="formText" for="commodity">Commodity</label>
							<input type="email" class="form-control" id="commodity" placeholder="Enter Commodity">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">HS Code</label>
							<input type="email" class="form-control" id="HS" placeholder="Enter HS Code">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Art of transport</label>
							<input type="email" class="form-control" id="HS" placeholder="">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Cargo Type</label>
							<input type="email" class="form-control" id="HS" placeholder="Enter number">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Weight of Container</label>
							<input type="email" class="form-control" id="HS" placeholder="ej: 120 x 120 x 190">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Number of package</label>
							<input type="email" class="form-control" id="HS" placeholder="1">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Dimensions</label>
							<input type="email" class="form-control" id="HS" placeholder="ej: 000 x 000 x 000 cm">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Total weight</label>
							<input type="email" class="form-control" id="HS" placeholder="ej: 500kg">
						</div>
					</div>
					<div class="col-xs-6">
						<div class="form-group">
							<label class="formText" for="HS">Trade</label>
							<select class="form-control">
								<option>Export</option> <option>Import </option> <option>Crosstrade</option>
							</select>
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Cargo ready date</label>
							<input type="text" class="form-control" id="HS" placeholder="Select date">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Pick up date</label>
							<input type="text" class="form-control" id="HS" placeholder="Select date">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Total value of cargo</label>
							<input type="text" class="form-control" id="HS" placeholder="Enter cargo value">
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Do you need Transport Insurance?</label>
							<select class="form-control">
								<option>No</option> <option>Yes </option>
							</select>
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Do you have regular shipments?</label>
							<select class="form-control">
								<option>No</option> <option>Yes </option>
							</select>
						</div>
						<div class="form-group">
							<label class="formText" for="HS">Total value of cargo</label>
							<input type="text" class="form-control" name="user-name" id="user-name" placeholder="Shipping details / Information ">
						</div>
						<a type="submit" class="btn btn-danger" href="{{ route('index.priceSelect') }}">CONTINUE</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--Fin del formulario-->


@endsection