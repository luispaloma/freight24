@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'Warehouse'])
<div class="container-fluid block-content">
	<div class="row main-grid">
		@include('layouts.form_sidebar_menu')
		<div class="col-sm-9">
			<div class="row">
				<div id="success"></div>
			</div>
			<form action="{{ route('warehousecontroller.save') }}" method="POST" novalidate id="contactForm" class="reply-form">
				{{ csrf_field() }}
				@if ( session('success') )
				<div class="row">
					<div class="col-sm-12">
					    <div class="alert alert-success">
					    	{{ session('success') }}
					    </div>
				    </div>
			    </div>
				@endif
				@if (count($errors) > 0)
				<div class="row">
					<div class="col-sm-12">
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					</div>
				</div>
				@endif
				<div class="row">
					<div class="col-xs-6">
						<h4>WAREHOUSE LOCATION</h4>
						<h5>Select country</h5>
						<div class="form-group">
							<select class="form-control" id="country" name="country">
								@foreach ($countries as $key => $country)
								<option value="{{ $country->id }}" @if ( old('country') == $country->id) selected @endif>{{ $country->name }}</option>
								@endforeach
							</select>
							<input type="text" class="form-control" placeholder="Enter city" id="city" name="city" value="{{ old('city') }}">
							<input type="text" class="form-control" placeholder="Zip Code"  id="zip_code" name="zip_code" value="{{ old('zip_code') }}">
						</div>
					</div>
					<div class="col-xs-6">
						<h4>MAX RADIUS</h4>
						<input type="text" class="form-control" placeholder="MAX. RADIUS 20 KM"  id="max_radius" name="max_radius" value="{{ old('max_radius') }}">
					</div>
				</div>
				<br>
				<br>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label class="formText" for="commodity">Commodity</label>
							<input type="text" class="form-control" id="commodity" name="commodity" placeholder="Enter Commodity" value="{{ old('commodity') }}">
						</div>
						<div class="form-group">
							<label class="formText" for="hs_code">HS Code</label>
							<input type="text" class="form-control" id="hs_code" name="hs_code" placeholder="Enter HS Code" value="{{ old('hs_code') }}">
						</div>
						<div class="form-group">
							<label class="formText">Type of package</label>
							<select class="form-control" id="type_package" name="type_package">
								<option value="Bag">Bag</option>
								<option value="Bale, compressed">Bale, compressed</option>
								<option value="Bundle">Bundle</option>
								<option value="Box">Box</option>
								<option value="Carton">Carton</option>
								<option value="Case">Case</option>
								<option value="Coil">Coil</option>
								<option value="Container">Container</option>
								<option value="Crate">Crate</option>
								<option value="Cylinder">Cylinder</option>
								<option value="Drum">Drum</option>
								<option value="Package">Package</option>
								<option value="Pallet">Pallet</option>
								<option value="Palletbox">Palletbox</option>
								<option value="Plate">Plate</option>
								<option value="Roll">Roll</option>
							</select>
						</div>
						<div class="form-group">
							<label class="formText" for="number_package">Number of package</label>
							<input type="text" class="form-control" id="number_package" name="number_package" placeholder="Enter number" value="{{ old('number_package') }}">
						</div>
						<div class="form-group">
							<label class="formText">Dimension</label>
							<input type="text" class="form-control SmallForm" id="dimension_1" name="dimension_1" value="{{ old('dimension_1') }}">x
							<input type="text" class="form-control SmallForm" id="dimension_2" name="dimension_2" value="{{ old('dimension_2') }}">x
							<input type="text" class="form-control SmallForm" id="dimension_3" name="dimension_3" value="{{ old('dimension_3') }}">
						</div>
						<div class="form-group">
							<label class="formText" for="Weight">Weight of cargo</label>
							<input type="email" class="form-control" id="weight_cargo" name="weight_cargo" placeholder="ej: 500 kg / each" value="{{ old('weight_cargo') }}">
						</div>
						<div class="form-group">
							<label class="formText" >Stackable</label>
							<select class="form-control" id="stackable" name="stackable">
								<option value="0">No</option>
								<option value="1">Yes </option>
							</select>
						</div>
						<div class="form-group">
							<label class="formText">Dangerous goods</label>
							<select class="form-control" id="dangerous_goods" name="dangerous_goods">
								<option value="0">No</option>
								<option value="1">Yes </option>
							</select>
							<p>(if yes -> below IMO Class, UN Number and Packaging Class has to be filled out)</p>
						</div>
						<div class="form-group">
							<label class="formText">IMO Class</label>
							<select class="form-control" id="imo_class" name="imo_class">
								<option value="1,1">1,1</option>
								<option value="1,2">1,2</option>
								<option value="1,3">1,3</option>
								<option value="1,4">1,4</option>
								<option value="1,5">1,5</option>
								<option value="1,6">1,6</option>
								<option value="2,1">2,1</option>
								<option value="2,2">2,2</option>
								<option value="2,3">2,3</option>
								<option value="3">3</option>
								<option value="4,1">4,1</option>
								<option value="4,2">4,2</option>
								<option value="4,3">4,3</option>
								<option value="5,1">5,1</option>
								<option value="5,2">5,2</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
							</select>
						</div>
						<div class="form-group">
							<label class="formText" for="un_number">UN Number</label>
							<input type="text" class="form-control" id="un_number" name="un_number" placeholder="Enter number" value="{{ old('un_number') }}">
						</div>
						<div class="form-group">
							<label class="formText" for="pack_class">Packaging Class</label>
							<input type="text" class="form-control" id="pack_class" name="pack_class" placeholder="Enter number" value="{{ old('pack_class') }}">
						</div>
					</div>
					<div class="col-xs-6">
						<div class="form-group">
							<label class="formText" for="start_storage">Start Storage date</label>
							<input type="text" class="form-control" id="start_storage" name="start_storage" placeholder="Select date" value="{{ old('start_storage') }}">
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" id="start_storage_unknown" name="start_storage_unknown"> If unknown click here
							</label>
						</div>
						<div class="form-group">
							<label class="formText" for="end_storage_date">End Storage date</label>
							<input type="text" class="form-control" id="end_storage_date" name="end_storage_date" placeholder="Select date" value="{{ old('end_storage_date') }}">
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" id="end_storage_date_unknown" name="end_storage_date_unknown"> If unknown click here
							</label>
							<div class="form-group">
								<label class="formText">Price per </label>
								<select class="form-control" id="price_pe" name="price_pe">
									<option value="Package">Package</option>
									<option value="m2">m2</option>
									<option value="m3">m3</option>
									<option value="tons">tons</option>
								</select>
							</div>
							<div class="form-group">
								<label class="formText">and duration </label>
								<select class="form-control" id="and_duration" name="and_duration">
									<option value="per day">per day</option>
									<option value="weekly">weekly</option>
									<option value="month">month</option>
									<option value="year">year</option>
								</select>
							</div>
							<div class="form-group">
								<label class="formText" for="value_goods">Total value of goods</label>
								<input type="text" class="form-control" id="value_goods" name="value_goods" placeholder="Enter cargo value" value="{{ old('value_goods') }}">
							</div>
							<div class="form-group">
								<label class="formText">Do you need Storage Insurance?</label>
								<select class="form-control" id="storage_insurance" name="storage_insurance">
									<option value="0">No</option>
									<option value="1">Yes </option>
								</select>
							</div>
							<div class="form-group">
								<label class="formText">Shipping details</label>
								<input type="text" class="form-control" name="shipping_details" id="shipping_details" placeholder="Shipping details / Information " value="{{ old('shipping_details') }}">
							</div>
							<input type="submit" class="btn btn-danger" value="CONTINUE">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection