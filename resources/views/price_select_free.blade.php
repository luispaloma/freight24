@extends('layouts.web_layout')
@section('content')
@include('layouts.page_header', ['page_title' => 'Prices'])

<div class="container-fluid inner-offset">
	<div class="hgroup text-center wow zoomIn" data-wow-delay="0.3s">
		<h2>History</h2>
		<h1>i-FREIGHT 24 - THE RIGHT CHOICE</h1>
	</div>

	<div class="container-fluid block-content">
	  <div class="row main-grid">
	    <div class="hgroup text-center wow zoomInUp" data-wow-delay="0.1s">
	    <h1>YOU HAVE FREE QUOTE!</h1>
	    <h2>Thank you for using i-Freight24 <br> you have <strong>4 quotes left</strong></h2>
	  </div>
	  </div>
	</div>
</div>

@endsection