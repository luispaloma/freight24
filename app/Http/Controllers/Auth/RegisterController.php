<?php

namespace freight24\Http\Controllers\Auth;

use freight24\User;
use freight24\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'company' => 'required|max:255',
            'address' => 'required|max:255',
            'city' => 'required|max:50',
            'plz' => 'required|max:50',
            'country' => 'required|max:50',
            'phone_number' => 'required|max:50',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'company' => $data['company'],
            'address' => $data['address'],
            'city' => $data['city'],
            'plz' => $data['plz'],
            'country' => $data['country'],
            'phone_number' => $data['phone_number'],
            'password' => bcrypt($data['password'])
        ]);
    }


    public function showRegistrationForm()
    {
        $data['title'] = 'register';
        return view('register', $data);
    }
}
