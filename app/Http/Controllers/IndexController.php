<?php

namespace freight24\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
		$data['title'] = 'I-Freight 24 (Under Construction)';
    	return view('index', $data);
    }

    public function contact()
    {
		$data['title'] = 'I-Freight 24 (Under Construction)';
    	return view('contact', $data);
    }

    public function feedback()
    {
		$data['title'] = 'I-Freight 24 (Under Construction)';
    	return view('feedback', $data);
    }

    public function filosophy()
    {
		$data['title'] = 'I-Freight 24 (Under Construction)';
    	return view('filosophy', $data);
    }

    public function history()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('history', $data);
    }

    public function air()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('air', $data);
    }

    public function road()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('road', $data);
    }

    public function sea()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('sea', $data);
    }

    public function fcl()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        $data['countries'] = $this->countries;
        return view('fcl', $data);
    }

    public function lcl()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        $data['countries'] = $this->countries;
        return view('lcl', $data);
    }

    public function log_in()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('log_in', $data);
    }

    public function new_user()
    {
		$data['title'] = 'I-Freight 24 (Under Construction)';
    	return view('new_user', $data);
    }

    public function road_ftl()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('road_ftl', $data);
    }

    public function road_ltl()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('road_ltl', $data);
    }

    public function warehouse()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('warehouse', $data);
    }

    public function for_shippers()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('for_shippers', $data);
    }

    public function about_us()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('about_us', $data);
    }

    public function for_carriers()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('for_carriers', $data);
    }

    public function price_select()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('price_select', $data);
    }

    public function price_select_free()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('price_select_free', $data);
    }

    public function terms()
    {
        $data['title'] = 'I-Freight 24 (Under Construction)';
        return view('terms', $data);
    }
}
