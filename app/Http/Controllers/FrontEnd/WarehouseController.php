<?php

namespace freight24\Http\Controllers\FrontEnd;

use freight24\Http\Controllers\Controller;
use Illuminate\Http\Request;
use freight24\Mail\ContactSent;
use Illuminate\Support\Facades\Mail;
use freight24\Country;
use freight24\Warehouse;
use Auth;
use Carbon\Carbon;

class WarehouseController extends Controller
{
    public function index()
    {
    	$data['title'] = 'I-Freight 24 (Under Construction)';
		$countries = Country::all();
        $data['countries'] = $countries;
        return view('warehouse', $data);
    }

    public function save(Request $request)
    {
    	$fields = $request->all();
    	$this->validate($request, [
			'country' => 'required|numeric',
			'city' => 'required|min:2|alpha_num',
			'zip_code' => 'required|min:2|alpha_num',
			'max_radius' => 'required|min:2|numeric',
			'commodity' => 'required|min:2|alpha_num',
			'hs_code' => 'required|min:2|alpha_num',
			'type_package' => 'required|min:2|alpha_num',
			'number_package' => 'required|min:2|numeric',
			'dimension_1' => 'required|min:2|alpha_num',
			'dimension_2' => 'required|min:2|alpha_num',
			'dimension_3' => 'required|min:2|alpha_num',
			'weight_cargo' => 'required|min:2|alpha_num',
			'stackable' => 'required|numeric',
			'dangerous_goods' => 'required|numeric',

			'start_storage' => 'required',
			'end_storage_date' => 'required',
			'price_pe' => 'required|min:2|alpha_num',
			'and_duration' => 'required|min:2',
			'value_goods' => 'required|min:2|alpha_num',
			'storage_insurance' => 'required',
			'shipping_details' => 'required|min:2|alpha_num',
		]);

		$warehouse = new Warehouse;
		$warehouse->user_id = Auth::user()->id;
		$warehouse->country = $fields['country'];
		$warehouse->city = $fields['city'];
		$warehouse->zip_code = $fields['zip_code'];
		$warehouse->max_radius = $fields['max_radius'];
		$warehouse->commodity = $fields['commodity'];
		$warehouse->hs_code = $fields['hs_code'];
		$warehouse->type_package = $fields['type_package'];
		$warehouse->number_package = $fields['number_package'];
		$warehouse->dimension_1 = $fields['dimension_1'];
		$warehouse->dimension_2 = $fields['dimension_2'];
		$warehouse->dimension_3 = $fields['dimension_3'];
		$warehouse->weight_cargo = $fields['weight_cargo'];
		$warehouse->stackable = $fields['stackable'];
		$warehouse->dangerous_goods = $fields['dangerous_goods'];
		$warehouse->imo_class = $fields['imo_class'];
		$warehouse->un_number = $fields['un_number'];
		$warehouse->pack_class = $fields['pack_class'];
		$warehouse->start_storage = $fields['start_storage'];
		if ( $warehouse->start_storage_unknown ) {
			$warehouse->start_storage_unknown = $fields['start_storage_unknown'];
		}
		$warehouse->end_storage_date = $fields['end_storage_date'];
		if ( $warehouse->end_storage_date_unknown ) {
			$warehouse->end_storage_date_unknown = $fields['end_storage_date_unknown'];
		}
		$warehouse->price_pe = $fields['price_pe'];
		$warehouse->and_duration = $fields['and_duration'];
		$warehouse->value_goods = $fields['value_goods'];
		$warehouse->start_storage = $fields['start_storage'];
		$warehouse->shipping_details = $fields['shipping_details'];
		$warehouse->shipping_details = $fields['shipping_details'];
		$warehouse->save();


        // Mail::to('luispaloma3@gmail.com')->send(new ContactSent($fields));

        return back()->with('success', 'Data sent.');
    }
}
