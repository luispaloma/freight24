<?php

namespace freight24\Http\Controllers\FrontEnd;

use freight24\Http\Controllers\Controller;
use Illuminate\Http\Request;
use freight24\Mail\ContactSent;
use Illuminate\Support\Facades\Mail;

class LclController extends Controller
{
    public function index()
    {
    	$data['title'] = 'I-Freight 24 (Under Construction)';
        $data['countries'] = $this->countries;
        return view('lcl', $data);
    }

    public function send_email(Request $request)
    {
    	$fields = $request->all();
    	$this->validate($request, [
			'user_name' => 'required|min:2|alpha',
			'user_email' => 'required|min:2|email',
			'user_lastname' => 'required|min:2|alpha',
			'user_phone' => 'required|min:2|numeric',
			'user_subject' => 'required|min:2',
			'user_message' => 'required|min:2'
		]);

        Mail::to('luispaloma3@gmail.com')->send(new ContactSent($fields));

        return redirect()->back();
    }
}
