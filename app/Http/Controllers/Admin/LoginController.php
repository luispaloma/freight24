<?php

namespace freight24\Http\Controllers\Admin;

use freight24\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

	use AuthenticatesUsers;

	protected $redirectPath = '/';


	protected function authenticated()
	{
		// return Redire route('admin.dashboard');
		return redirect()->route('admin.dashboard');
	}

	protected function guard()
	{
		return Auth::guard('admins');
	}

	public function showLoginForm()
	{
		return view('Admin.index');
	}

	public function logout(Request $request)
	{
		$this->guard()->logout();
		$request->session()->flush();
		$request->session()->regenerate();
		return redirect()->route('admin.index');

	}
}
