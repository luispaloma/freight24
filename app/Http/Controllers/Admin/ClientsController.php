<?php

namespace freight24\Http\Controllers\Admin;

use freight24\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClientsController extends Controller
{

	public function index()
	{
		return view('Admin.clients.index');
	}

	public function sea_freight()
	{
		return view('Admin.clients.sea_freight');
	}

	public function air_freight()
	{
		return view('Admin.clients.air_freight');
	}
}
