<?php

namespace freight24\Http\Controllers\Admin;

use freight24\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    public function index()
    {
    	return view('admin.messages.index');
    }
}
