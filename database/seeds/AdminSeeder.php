<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        freight24\Admin::create(['first_name' => 'Luis', 'last_name' => 'Palomá', 'email' => 'luispaloma3@gmail.com', 'password' => bcrypt('123456')]);
        freight24\Admin::create(['first_name' => 'Admin', 'last_name' => 'User', 'email' => 'info@i-freight24.com', 'password' => bcrypt('123456')]);
    }
}
