<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        freight24\User::create([
        		'first_name' => 'Luis',
        		'last_name' => 'Palomá',
        		'email' => 'luispaloma3@gmail.com',
        		'company' => 'Merqueo',
        		'address' => 'Merqueo',
        		'city' => 'Bogotá',
        		'plz' => 'asdf',
        		'country' => 'Colombia',
        		'phone_number' => '3042066328',
        		'carrier_type' => 'Shipper',
        		'carrier_type' => 'Shipper',
        		'free_quotes' => 4,
        		'password' => bcrypt('123456')
    		]);
    }
}
