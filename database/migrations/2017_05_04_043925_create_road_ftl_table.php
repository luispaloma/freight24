<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoadFtlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('road_ftl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');

            $table->string('commodity');
            $table->string('hs_code');
            $table->string('type_truck');
            $table->unsignedInteger('package_number');
            $table->string('dimension');
            $table->string('cargo_weight');
            $table->enum('stackable', ['No', 'Yes']);
            $table->enum('dangerous_goods', ['No', 'Yes']);
            $table->string('total_weight');
            $table->string('total_volume');
            $table->enum('trade', ['Export', 'Import', 'Crosstrade']);
            $table->dateTime('cargo_ready_date');
            $table->dateTime('pickup_date');
            $table->string('cargo_total');
            $table->enum('transport_insurance', ['No', 'Yes']);
            $table->enum('regular_shipments', ['No', 'Yes']);
            $table->text('shipping_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('road_ftl');
    }
}
