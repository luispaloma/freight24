<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComapnyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('id_number');
            $table->string('cargo_type', 55)->unique();
            $table->string('package_number', 25);
            $table->string('cargo_type_2', 55);
            $table->float('weight_available');
            $table->float('volume_available');
            $table->string('imo_class');
            $table->string('un_number');
            $table->string('trade');
            $table->string('commodity');
            $table->string('hs_code');
            $table->string('shipping_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
