<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLclTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lcl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('loading_city');
            $table->string('loading_zip_code');
            $table->string('loading_location_type');
            $table->string('discharge_city');
            $table->string('discharge_zip_code');
            $table->string('discharge_location_type');
            $table->string('commodity');
            $table->string('hs_code');
            $table->string('type_package');
            $table->unsignedInteger('number_package');
            $table->string('dimension');
            $table->string('weight');
            $table->enum('stackable', ['Export', 'Import', 'Crosstrade']);
            $table->enum('dangerous_goods', ['No', 'Yes']);
            $table->dateTime('cargo_ready_date');
            $table->dateTime('ready_date');
            $table->dateTime('pickup_date');
            $table->unsignedInteger('total_value');
            $table->enum('transport_insurance', ['No', 'Yes']);
            $table->enum('regular_shipments', ['No', 'Yes']);
            $table->text('shipping_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lcl');
    }
}
