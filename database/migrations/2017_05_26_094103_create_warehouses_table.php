<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('country');
            $table->string('city');
            $table->string('zip_code');
            $table->unsignedInteger('max_radius');

            $table->string('commodity');
            $table->string('hs_code');
            $table->string('type_package');
            $table->unsignedInteger('number_package');
            $table->string('dimension_1');
            $table->string('dimension_2');
            $table->string('dimension_3');
            $table->string('weight_cargo');
            $table->unsignedTinyInteger('stackable');
            $table->unsignedTinyInteger('dangerous_goods');
            $table->string('imo_class');
            $table->string('un_number');
            $table->string('pack_class');
            $table->dateTime('start_storage');
            $table->unsignedTinyInteger('start_storage_unknown');
            $table->dateTime('end_storage_date');
            $table->unsignedTinyInteger('end_storage_date_unknown');
            $table->string('price_pe');
            $table->string('and_duration');
            $table->string('value_goods');
            $table->unsignedTinyInteger('storage_insurance');
            $table->string('shipping_details');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouses');
    }
}
