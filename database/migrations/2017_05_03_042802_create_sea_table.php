<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sea', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('loading_city');
            $table->string('loading_zip_code');
            $table->string('loading_location_type');
            $table->string('loading_country');
            $table->string('loading_port');
            $table->tinyInteger('loading_on_board');

            $table->string('discharge_city');
            $table->string('discharge_zip_code');
            $table->string('discharge_location_type');
            $table->string('discharge_country');
            $table->string('discharge_port');
            $table->tinyInteger('discharge_on_board');

            $table->string('commodity');
            $table->string('hs_code');
            $table->string('art_transport');
            $table->string('cargo_type');
            $table->string('container_weight');
            $table->unsignedInteger('package_number');
            $table->string('container_dimension');
            $table->string('total_weight');

            $table->enum('trade', ['Export', 'Import', 'Crosstrade']);
            $table->dateTime('cargo_ready_date');
            $table->dateTime('pickup_date');
            $table->string('cargo_total');
            $table->enum('transport_insurance', ['No', 'Yes']);
            $table->enum('regular_shipments', ['No', 'Yes']);
            $table->unsignedInteger('total_value');
            $table->text('shipping_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sea');
    }
}
