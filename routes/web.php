<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/*=========================================================
=            Admin routes index, login, logout            =
=========================================================*/
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
	Route::get('/', [
		'as'   => 'admin.index',
		'uses' => 'LoginController@showLoginForm'
	]);

	Route::post('/login', [
		'as'   => 'admin.login',
		'uses' => 'LoginController@login'
	]);

	Route::get('/logout', [
		'as'   => 'admin.logout',
		'uses' => 'LoginController@logout'
	]);

});

/*==============================================
=            Admin routes dashboard            =
==============================================*/
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
	Route::get('/dashboard', [
		'as'   => 'admin.dashboard',
		'uses' => 'DashboradController@index'
	])->middleware('auth:admins');
});

/*============================================
=            Admin routes company            =
============================================*/
Route::group(['prefix' => 'admin/company', 'namespace' => 'Admin', 'middleware' => 'auth:admins'], function() {
	Route::get('/', [
		'as'   => 'admin.company',
		'uses' => 'CompanyController@index'
	]);
	Route::post('/save', [
		'as'   => 'admin.company.save',
		'uses' => 'CompanyController@save'
	]);
});

/*============================================
=            Admin routes clients            =
============================================*/
Route::group(['prefix' => 'admin/clients', 'namespace' => 'Admin', 'middleware' => 'auth:admins'], function() {
	Route::get('/', [
		'as'   => 'admin.clients',
		'uses' => 'ClientsController@index'
	]);

	Route::get('/sea-freight', [
		'as'   => 'admin.seaFreight',
		'uses' => 'ClientsController@sea_freight'
	]);
});

/*=============================================
=            Admin routes messages            =
=============================================*/
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth:admins'], function() {
	Route::get('/messages', [
		'as'   => 'admin.messages',
		'uses' => 'MessagesController@index'
	]);
});


/*====================================
=            Front Routes            =
====================================*/



/*==========================================
=            Login and register            =
==========================================*/
Route::group(['prefix' => 'users', 'namespace' => 'Auth', 'middleware' => 'auth:users'], function() {
	Route::get('/log-in', [
		'as'   => 'loginController.logIn',
		'uses' => 'LoginController@log_in'
	]);

	Route::get('/register', [
		'as'   => 'registerController.register',
		'uses' => 'RegisterController@register'
	]);
});





/*=============================
=            Index            =
=============================*/

Route::get('/', [
	'as'   => 'index',
	'uses' => 'IndexController@index'
]);

//Contact Page
Route::group(['prefix' => 'contact'], function() {
	Route::get('/', [
		'as'   => 'contactController.contact',
		'uses' => 'FrontEnd\ContactController@index'
	]);
	Route::post('/', [
		'as'   => 'contactController.SendMessage',
		'uses' => 'FrontEnd\ContactController@send_message'
	]);
});
//--Contact Page

//lcl
Route::group(['prefix' => 'lcl', 'middleware' => 'auth'], function() {
	Route::get('/', [
		'as'   => 'lclcontroller.index',
		'uses' => 'FrontEnd\LclController@index'
	]);
});
//--lcl

//warehouse
Route::group(['prefix' => 'warehouse', 'middleware' => 'auth'], function() {
	Route::get('/', [
		'as'   => 'warehousecontroller.index',
		'uses' => 'FrontEnd\WarehouseController@index'
	]);
	Route::post('save', [
		'as'   => 'warehousecontroller.save',
		'uses' => 'FrontEnd\WarehouseController@save'
	]);
});

Route::get('/feedback', [
	'as'   => 'index.feedback',
	'uses' => 'IndexController@feedback'
]);

Route::get('/filosophy', [
	'as'   => 'index.filosophy',
	'uses' => 'IndexController@filosophy'
]);

Route::get('/history', [
	'as'   => 'index.history',
	'uses' => 'IndexController@history'
]);

Route::get('/air', [
	'as'   => 'index.air',
	'uses' => 'IndexController@air'
]);

Route::get('/road', [
	'as'   => 'index.road',
	'uses' => 'IndexController@road'
]);

Route::get('/sea', [
	'as'   => 'index.sea',
	'uses' => 'IndexController@sea'
]);

Route::get('/fcl', [
	'as'   => 'index.fcl',
	'uses' => 'IndexController@fcl'
]);

Route::get('/new-user', [
	'as'   => 'index.newUser',
	'uses' => 'IndexController@new_user'
]);

Route::get('/road-ftl', [
	'as'   => 'index.road_ftl',
	'uses' => 'IndexController@road_ftl'
]);

Route::get('/road-ltl', [
	'as'   => 'index.road_ltl',
	'uses' => 'IndexController@road_ltl'
]);

Route::get('/for-shippers', [
	'as'   => 'index.forShippers',
	'uses' => 'IndexController@for_shippers'
]);

Route::get('/about-us', [
	'as'   => 'index.aboutUs',
	'uses' => 'IndexController@about_us'
]);

Route::get('/for-carriers', [
	'as'   => 'index.forCarriers',
	'uses' => 'IndexController@for_carriers'
]);

Route::get('/price-select', [
	'as'   => 'index.priceSelect',
	'uses' => 'IndexController@price_select'
]);

Route::get('/rail', [
	'as'   => 'index.rail',
	'uses' => 'IndexController@rail'
]);

Route::get('/price-select-free', [
	'as'   => 'index.priceSelectFree',
	'uses' => 'IndexController@price_select_free'
]);

Route::get('/terms', [
	'as'   => 'index.terms',
	'uses' => 'IndexController@terms'
]);